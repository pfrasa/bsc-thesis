.PHONY = open clean

out/thesis.pdf: out out/thesis.tex out/introduction.tex out/fundamental.tex out/mor.tex out/slfp.tex out/security.tex out/discussion.tex out/bibliography.bib out/title.tex out/metadata.tex out/declaration.tex
	cd out && latexmk -xelatex thesis.tex

out:
	mkdir -p out

out/thesis.tex: out thesis.tex
	cp thesis.tex out/

out/title.tex: out title.tex
	cp title.tex out/

out/metadata.tex: out metadata.tex
	cp metadata.tex out/

out/bibliography.bib: out bibliography.bib
	cp bibliography.bib out/

out/declaration.tex: out src/declaration.md
	pandoc src/declaration.md -o out/declaration.tex

out/introduction.tex: out src/introduction.md
	pandoc src/introduction.md -o out/introduction.tex

out/fundamental.tex: out src/fundamental.md
	pandoc src/fundamental.md -o out/fundamental.tex

out/mor.tex: out src/mor.md
	pandoc src/mor.md -o out/mor.tex

out/slfp.tex: out src/slfp.md
	pandoc src/slfp.md -o out/slfp.tex

out/security.tex: out src/security.md
	pandoc src/security.md -o out/security.tex

out/discussion.tex: out src/discussion.md
	pandoc src/discussion.md -o out/discussion.tex

open: out/thesis.pdf
	xdg-open out/thesis.pdf

clean:
	rm -rf out
