**Selbständigkeitserklärung**

Ich erkläre, dass ich die Bachelorarbeit selbstständig und ohne unzulässige
Inanspruchnahme Dritter verfasst habe. Ich habe dabei nur die angegebenen
Quellen und Hilfsmittel verwendet und die aus diesen wörtlich oder sinngemäß
entnommenen Stellen als solche kenntlich gemacht. Die Versicherung
selbstständiger Arbeit gilt auch für enthaltene Zeichnungen, Skizzen oder
graphische Darstellungen. Die Bachelorarbeit wurde bisher in gleicher oder
ähnlicher Form weder derselben noch einer anderen Prüfungsbehörde vorgelegt
und auch nicht veröffentlicht. Mit der Abgabe der elektronischen Fassung der
endgültigen Version der Bachelorarbeit nehme ich zur Kenntnis, dass diese mit
Hilfe eines Plagiatserkennungsdienstes auf enthaltene Plagiate geprüft werden
kann und ausschließlich für Prüfungszwecke gespeichert wird.

\begin{tabular}{@{}p{1in}p{4in}@{}}
\\
\\
\textbf{Datum}: & \hrulefill \\
\\
\\
\\
\textbf{Unterschrift}: & \hrulefill
\end{tabular}
