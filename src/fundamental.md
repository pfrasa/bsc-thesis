# Fundamental concepts {#fundamental}
In this section, we will introduce some of the fundamental concepts of both public key cryptography and group theory that we will need in the rest of the thesis.

## Cryptography
**Cryptography** is the field of mathematics that "involves the study of mathematical techniques for securing digital information, systems, and distributed computations against adversarial attacks" \cite[p.~3]{crypto_book}. Central to the study of cryptography are *encryption schemes*, which are also referred to as *cryptosystems* or *ciphers*. In particular, we will be concerned with *public-key cryptosystems*. We will mostly follow the treatment as given in \cite{crypto_book} in this section, but with some simplifications.

### Public-Key Cryptography {#pubkey}

We introduce a type of cipher designed to secure the communication between two parties. As is customary in cryptography, we will use the names Alice and Bob for the sender and the receiver of some message, respectively.

\begin{defn}\label{pubkey-defn}
A \textbf{public-key} cipher is a tuple $\Pi = (\mathcal{M}, \mathcal{C}, \mathcal{K}, \mathrm{Enc}, \mathrm{Dec})$ such that
\begin{itemize}
\item $\mathcal{M}$ is a set of messages called \emph{plaintexts}.
\item $\mathcal{C}$ is a set of messages called \emph{ciphertexts}.
\item The elements of $\mathcal{K}$ are called \emph{key-pairs}, and $\mathcal{K} \subset \mathcal{K}_p \times \mathcal{K}_s$ where $\mathcal{K}_p$ is a set of so-called \emph{public keys} and $\mathcal{K}_s$ is a set of so-called \emph{private keys}.
\item $\mathrm{Enc} : \mathcal{M} \times \mathcal{K}_p \rightarrow \mathcal{C}$ is called the \emph{encryption function}.
\item $\mathrm{Dec} : \mathcal{C} \times \mathcal{K}_s \rightarrow \mathcal{M}$ is called the \emph{decryption function}.
\item For all $m \in \mathcal{M}$ and all $(k_p, k_s) \in \mathcal{K}$ we have that $\mathrm{Dec}(\mathrm{Enc}(m, k_p), k_s) = m$.
\end{itemize}
\end{defn}

This definition allows for arbitrary sets to be used as the plaintext, ciphertext or key spaces. Of course, for a cipher to be implemented in practice, elements of these sets will have to be encoded as finite sequences of bits in some form.

The cipher can now be used as follows: Bob generates a key-pair $(k_p, k_s) \in \mathcal{K}$, keeps the private key $k_s$ secret and publishes the public key $k_p$ under his own name. Alice (or indeed any other participant that wants to communicate with Bob) can now encrypt a message using the public key and the encryption function. This message will then be sent to Bob who is able to decrypt the message using the private key and the decryption function. We note that, in practice, the encryption function will actually be a random function, so it may not return the same ciphertext for a given message and public key each time (see also theorem \ref{deterministic-insecure}); for simplicity, we will still refer to it using regular function notation. Decryption, however, always has to be deterministic.

Public-key cryptosystems are also called *asymmetric* ciphers and are contrasted by **symmetric** (or *private-key*) schemes, in which, instead of a pair of keys, a single key is used both for encryption and decryption. Symmetric ciphers have the big disadvantage that, in order for them to be secure, the key needs to be securely shared between Alice and Bob without it being exposed to any third party. By contrast, in a public-key scheme Bob only needs to make sure to keep the private key secret. However, encryption and decryption in public-key ciphers are typically slower by about one to two orders of magnitude when compared to symmetric encryption which can matter a lot when transmitting large amounts of data. In practice, *hybrid* systems are often used where, for example, a public-key cipher is used to securely exchange a key that is then used for encrypting the actual message with a symmetric cipher (a similar principle is used e.g. in TLS, see \cite[pp.~479--481]{crypto_book}).

There are a number of public-key cryptosystems that are commonly used in practice. One of the most common choices is *RSA* (which comes in a number of variants, such as RSA-1024 and RSA-2048, depending on the bit-size of the keys). Many other ciphers, such as El-Gamal (see section \ref{el-gamal}), can be applied to a variety of settings. Recently, there has been a lot of interest in *elliptic curve cryptography* (ECC), in which one operates on groups that consist of points on elliptic curves. ECC schemes offer faster decryption with smaller key sizes than other schemes for similar levels of security. A number of different concrete ciphers can be implemented on top of ECC.

When analysing a public-key cryptosystem, there are generally three things that we care about: The system needs to be *correct*, *efficient* and *secure*. Correctness refers to the fact that decrypting a message should always result in the original message no matter what---this requirement was already part of our original definition \ref{pubkey-defn}. Efficiency refers to the encryption and decryption operations and is important in order for the scheme to be useful in practice. Analysing the efficiency of algorithms is a well-understood practice in computer science and therefore poses no significant hurdles. In the next section we will discuss that, in the description of a cryptosystem, we will often introduce the bit-size of the keys as a parameter. With that in mind, we will generally require that encryption and decryption run in polynomial time with respect to that parameter.

On the other hand, showing that a cryptosystem is actually secure requires significantly more work. We will address this issue in the following section.

### Security of Public-Key Cryptosystems {#pubkey-security}

For most of its existence, cryptography has been treated more as an art than a science. Both developing and breaking cryptosystems used to rely mostly on intuition and luck, and systems were considered secure if they hadn't been broken in practice for a longer period of time. While some of this remains true, in more recent decades more rigorous approaches to cryptography have been developed that aim to define more carefully what it means for a cipher to be secure and to find methods to formally prove security in practice. We will now provide a brief overview of such approaches as they relate to public-key cryptosystems without going into significant detail (see \cite{crypto_book} for a comprehensive treatment).

Already in the 1880s, Auguste Kerckhoff argued that *the security of a cryptosystem should only ever depend on the (private) key being kept secret*. This principle is now referred to as **Kerckhoff's principle** and is still generally adhered to to this day. In particular, one should always assume that a potential attacker knows the exact details of the cipher being used and not try to keep it secret. One reason for this is that it is much easier to keep a key secret than an entire algorithm and to change an accidentally exposed key rather than having to switch to a completely different cipher. However, this principle immediately implies a very simple attack on any system: An attacker can simply try decrypting a ciphertext with every possible private key. If the original plaintext message is a message in a language such as English, typically only one of the decryptions will be meaningful and can therefore be identified as the plaintext. In order to render such a **brute-force attack** infeasible in practice, the set of all possible key pairs needs to be large. However, while this is certainly a necessary condition for the security of a system, it is not a sufficient one.

The first hurdle in the analysis of a cipher's security is finding a formal definition of what it means to be *secure*. One very simple definition of security asserts that an attacker should never be able to reconstruct the full plaintext from a ciphertext without knowing the private key. However, this definition is unlikely to be suitable in practice since even just being able to retrieve, for example, half of the plaintext would in many cases already constitute an unacceptable breach of confidentiality. A definition of security also needs to be explicit about the types of attacks that it is intended to deal with. An attacker might have different resources available at their disposal and a particular system may be secure with respect to some of these attacks but not to others---depending on the intended usage of the system this may or may not be a problem in practice. The most common types of attacks are:

- **Ciphertext-only attack**: In this attack, the attacker is able to see one particular ciphertext and is trying to recover the original plaintext.
- **Known-plaintext attack**: Here, the attacker already knows one plaintext-ciphertext pair and uses that knowledge to decrypt a second ciphertext. Such types of attacks are common because many types of communication include redundancies such as greetings, particular message headers, etc. that can be successfully guessed by an attacker.
- **Chosen-plaintext attack**: The attacker can get the encryption of any plaintext of their choosing and uses that to decrypt any additional ciphertexts they intercept. This type of attack always needs to be considered in the case of public-key encryption since the attacker can encrypt any message by using the public key.
- **Chosen-ciphertext attack**: The attacker can choose a particular ciphertext and is able to get its decryption. They will then use this knowledge to decrypt another ciphertext that they intercepted.

In order to develop formal notions of security, we will need to employ the language of probability theory. Within a given context, we will assume that there are random variables $M, C, K$ associated with distributions over the plaintext, ciphertext and key spaces. The notation $P(M=m)$ will denote the probability that the plaintext message is $m$. This probability does not depend on the cipher in question but on some prior assumptions about the probability of each message (for example, in a given context we may assume that a message in English is more likely than a message in French or a message that is just a random sequence of characters). We denote with $P(K=k)$ the probability of having chosen a particular key or key-pair $k$---in most cases, one can assume that $k$ was chosen uniformly at random. Finally, the probability $P(C=c)$ of the ciphertext being $c$ ultimately depends on the probability distributions of $M$, $K$ and any random choices made by the encryption algorithm.

For symmetric cryptosystems, there is a very simple definition of security that seems promising at first:
\begin{defn}\label{perfect-secrecy}
We say that a cipher has \textbf{perfect secrecy} if, for any prior probability distribution $M$ over $\mathcal{M}$, any message $m \in \mathcal{M}$ and any ciphertext $c \in \mathcal{C}$ with $P(C=c) > 0$
$$
P(M=m|C=c) = P(M=m).
$$
\end{defn}
In other words, this definition captures the intuitive notion that a ciphertext should not reveal any additional information about the plaintext.

Unfortunately, there are at least two major problems with this definition. The first is that it can be shown that for any cipher with perfect secrecy, the key must be at least as long as the message. Such a system is therefore rarely useful in practice since it requires the parties to have exchanged a key of significant length through some other means (e.g. in person) beforehand. One system that provably satisfies perfect secrecy is called the *One-Time Pad* and, while it has occasionally been used e.g. between the US and the USSR during the Cold War, is not really suitable for most modern encryption needs.

The second problem with perfect secrecy is that no analogue definition can exist for public-key encryption schemes. The reason is that we have to take into account that the attacker doesn't only know the ciphertext but also the public key, so a more realistic requirement would be something like $P(M=m|C=c \wedge K_p = k_p) = P(M=m)$ for some distribution $K_p$ over the public key space. However, the left side of this equation can be zero even when the right side is not: For a given public key, the set of possible encryptions of a particular message $m$ (remember that encryption may be probabilistic) doesn't contain all possible ciphertexts---in particular, it cannot contain any of the valid encryptions of another message $m^\prime$ with the same public key since otherwise decryption would be impossible.

Given these two shortcomings, we require a more practical definition of security that also works for public-key cryptosystems. To do so, we first introduce a certain type of *experiment*: Assume that $\mathcal{A}$ is any algorithm (we will call it an *adversary*). $\mathcal{A}$ chooses two messages $m_0, m_1 \in \mathcal{M}$. The experiment will then choose either of $m_0$ or $m_1$ with equal probability and compute its encryption $c$ using a particular cryptosystem. It can be shown that a symmetric cryptosystem satisfies definition \ref{perfect-secrecy} if and only if *no* adversary $\mathcal{A}$ can determine whether $c$ is an encryption of $m_0$ or $m_1$ with probability more than $\frac{1}{2}$ (i.e. better than by just guessing) \cite[p.~31]{crypto_book}. If we weaken some of the conditions in this experiment, we will be able to provide a reasonable security definition for public-key ciphers.

The first thing we will do is to not consider *any* adversary $\mathcal{A}$, but only such ones that run *efficiently* (this reflects the fact that real-world adversaries don't have unlimited computing resources). There are essentially two different ways of capturing what this means: one can either calculate how much time (or computing cycles) it would require for an adversary to distinguish $m_0$ from $m_1$ and compare this with some concrete assumptions about the power of some attacker; or one can parameterise the cryptosystem by some parameter $n$ (typically the length of the key in bits) and describe the runtime of a successful attack as a function of $n$. In this second approach, *efficient* then means that the attack must run in (probabilistic) *polynomial time* with respect to $n$. Often, this second asymptotic approach can be used to prove the security of a parameterised algorithm, while the first, more concrete, approach can then be employed to determine which key length can be considered secure at any given point in time (see \cite[pp.~43--51]{crypto_book} for more details).

The second change is that we will allow an adversary $\mathcal{A}$ to correctly guess the message with a probability slightly above $\frac{1}{2}$. In particular, that probability can be $\frac{1}{2} + f(n)$ where $f$ is a function considered *negligible* in (the bit-size) $n$ (formally, we require that, for every $c \in \mathbb{N}$, there exist a $n_0 \in \mathbb{N}$ such that $f(n) < n^{-c}$ for all $n \geq n_0$). The functions $f(n)=2^{-n}$ and $f(n) = n^{-\log_2 n}$ are examples of negligible functions. They approach zero quite rapidly, for example $2^{-256} \approx 8.6\cdot 10^{-78}$.

Following \cite[pp.~379--386]{crypto_book}, we will now provide a precise and useful definition of security in the case of public-key encryption. Let's assume that we are given a public-key cryptosystem $\Pi_n = (\mathcal{M}_n, \mathcal{C}_n, \mathcal{K}_n, \mathrm{Enc}_n, \mathrm{Dec}_n)$ parameterised by $n$ and an adversary $\mathcal{A}$.  We can define the experiment $E[\Pi, \mathcal{A}, n]$ as follows:

\pagebreak

1. A key-pair $(k_p, k_s)$ is randomly generated in $\Pi_n$.
2. $\mathcal{A}$ is given $k_p$ and outputs two messages $m_0, m_1$ of equal length.
3. $b \in \{0, 1\}$ is chosen uniformly at random and $c = \mathrm{Enc}_n(m_b, k_p)$ is computed and sent to $\mathcal{A}$.
4. $\mathcal{A}$ outputs $b^\prime \in \{0, 1\}$.
5. The outcome of the experiment is $1$ if $b=b^\prime$ and $0$ otherwise.

\begin{defn}\label{cpa-secure}
A public-key cipher $\Pi$ parameterised by $n$ is called \textbf{CPA-secure} if for every adversary $\mathcal{A}$ that runs in polynomial time with respect to $n$, there exists some negligible $f$ such that
$$
P(E[\Pi, \mathcal{A}, n] = 1) \leq \frac{1}{2} + f(n)
$$
for all $n$.
\end{defn}

This definition states that, if a cipher is CPA-secure, an adversary cannot practically distinguish the encryption of two messages, even when given the public key *before* choosing a message pair. The name CPA-secure derives from the fact that such a cryptosystem can be shown to be secure (under a suitable definition) against chosen-plaintext attacks. Even better, it will also be provably secure when multiple messages are encrypted with the same public key. This indicates that CPA-security is in fact a good definition of security for public-key cryptosystems. Unfortunately, the following is also true:

\begin{theorem}[{\cite[p.~380]{crypto_book}}]\label{deterministic-insecure}
No public-key cryptosystem in which encryption is deterministic can be CPA-secure.
\end{theorem}

A practical example of this limitation can be seen when the set of messages to encrypt is small. In that case, an adversary can simply encrypt every possible message with the public key. If encryption is deterministic, the adversary can compare a given ciphertext with all computed encryptions to determine the original plaintext.

If we want to build a secure cipher, then definition \ref{cpa-secure} implies that it must be computationally infeasible to gain information about the plaintext, and in particular, to decrypt a ciphertext, without knowing the private key. However, one generally won't be able to prove that any real-world cipher satisfies this requirement *unconditionally*. Instead, one relies on assumptions about the difficulty of certain problems in mathematics which are *assumed* to be difficult to solve (in the sense that no polynomial-time algorithms exist). The aim in modern cryptography is therefore to show that a particular public-key cryptosystem is CPA-secure *if* a particular such problem cannot be solved by an efficient algorithm. Such assumptions could turn out to be wrong but since these are typically problems that have been studied for decades or longer, many mathematicians are quite confident that they are not. In the case of RSA, we rely on assumptions related to the difficulty of factoring large integers. In this thesis, the discrete logarithm problem that we will introduce in section \ref{group-problems} will play a prominent role.

## Group Theory {#grouptheory}
Group theory is concerned with *groups* and, in particular, with relations between groups as expressed by *homomorphisms*. We now introduce some of the fundamental concepts that will be needed later. None of the definitions, theorems or proofs in this section can be considered original and they can readily be found in standard treatments of these fields, such as \cite{dummit_foote}. We will also omit some proofs as long as they don't seem overly relevant to the rest of the thesis.

### Groups and homomorphisms

\begin{defn}
A \textbf{group} $(G,\cdot)$ consists of a set $G$ together with an \textbf{operation}, i.e. a function $\cdot : G \times G \longrightarrow G$ that satisfies the following properties:

\begin{itemize}
\item The operation is \emph{associative}, i.e. we have $g\cdot(h\cdot l) = (g\cdot h)\cdot l$ for all $g,h,l \in G$,
\item the set $G$ contains an \emph{identity} of the operation (commonly denoted $e$) such that $g\cdot e = e\cdot g = g$ for all $g \in G$, and
\item for every $g \in G$, there is an element $h\in G$ such that $g\cdot h = h\cdot g = e$; this element is called the \emph{inverse} of $g$ and is denoted $g^{-1}$.
\end{itemize}

If, additionally, the relation $g\cdot h = h\cdot g$ is true for all $g, h \in G$, we will say that the group is commutative or \textbf{abelian}.
\end{defn}

Since every group contains an identity, groups can't be empty. The smallest group is therefore the group that contains only the identity and where the group operation is defined such that $e\cdot e = e$.

We can, in theory, use any symbol we like for the group operation. In practice, whenever we talk about groups abstractly (instead of considering a specific group) it's most common to use *multiplicative* notation with the operation $\cdot$. Moreover, the symbol will often be omitted between elements, so that we will write $gh$ instead of $g\cdot h$. We will also be using the notational conventions of writing $g^n := g\cdot\ldots\cdot g$ ($n$ times) for all $n \in \mathbb{N}_0$ and $g^n = (g^{-n})^{-1}$ for all $n \in \mathbb{Z}\setminus\mathbb{N}_0$; in particular, $g^0 = e$. We note that the usual exponent laws such as $g^n g^m = g^{n+m}$ and $(g^n)^m = g^{nm}$ apply.

Groups arise naturally in many areas of mathematics. For example, the set of all real numbers without zero form an abelian group $(\mathbb{R}^\times, \cdot)$ under multiplication, as do the integers under addition, $(\mathbb{Z}, +)$ (see also section \ref{group-examples} for more examples). We will not always precisely distinguish between a group and its underlying set, especially when the corresponding group operation can be inferred from context---for example, the integers do not form a group under multiplication, so we will often write $\mathbb{Z}$ instead of $(\mathbb{Z},+)$.

\begin{sloppypar}
A \textbf{group homomorphism} $\varphi : G \rightarrow H$ between two groups $G$ and $H$ is a map that satisfies $\varphi(gg^\prime) = \varphi(g)\varphi(g^\prime)$ for any elements $g, g^\prime \in G$. For example, $\exp$ is a homomorphism between $\mathbb{Z}$ and $\mathbb{R}^\times$. We call the set ${\mathrm{ker}(\varphi) := \{ g \in G \mid \varphi(g) = e \}}$ the \textbf{kernel} of $\varphi$. If $\varphi$ is also a bijection, we will call it an \textbf{isomorphism}. The map $\exp$ is not an isomorphism between $\mathbb{Z}$ and $\mathbb{R}^\times$, as it's not surjective, but it is an isomorphism between $\mathbb{Z}$ and the group of positive real numbers under multiplication, $\mathbb{R}^+$. Groups between which an isomorphism exists are called \textbf{isomorphic} and for the purposes of group theory, they are treated as essentially the same. An isomorphism between a group and itself is called an \textbf{automorphism}. The map $\varphi : x \mapsto -x$ is an automorphism of $\mathbb{Z}$.
\end{sloppypar}

The following theorem shows that homomorphisms respect the group structure of their respective groups:
\begin{theorem}
Let $\varphi : G \rightarrow H$ be a homomorphism. Then:
\begin{enumerate}[label=(\alph*)]
\item $\varphi(e) = e$.
\item $\varphi(g^{-1}) = \varphi(g)^{-1}$ for all $g \in G$.
\end{enumerate}
\end{theorem}

If $H$ is a subset of a group $G$ and if $H$ is itself a group *under the same operation*, then we call $H$ a **subgroup** of $G$ and write $H < G$. The group $\mathbb{R}^+$ of positive real numbers is a subgroup of $\mathbb{R}^\times$ and the group $n\mathbb{Z}$ of all integer multiples of $n$ is a subgroup of $\mathbb{Z}$. We note the following theorem:
\begin{theorem}
If $H \subset G$ and $G$ is a group, then $H$ is a subgroup of $G$ if and only if $h_1h_2^{-1} \in H$ for all $h_1, h_2 \in H$.
\end{theorem}

If $H$ is a subgroup of $G$ and if $ghg^{-1} \in H$ for all $g \in G$ and all $h \in H$, then we call $H$ a **normal subgroup**, and denote this by $H \vartriangleleft G$. We call $ghg^{-1}$ the **conjugation** of $h$ by $g$, so another way of saying the same thing is that a normal subgroup is a subgroup which is closed by conjugation under arbitrary elements of the original group. In abelian groups, $ghg^{-1} = h$, so every subgroup is a normal subgroup. The following theorem establishes a deep relationship between homomorphisms and subgroups:
\pagebreak
\begin{theorem}\label{homo-subgroup}
\leavevmode
\begin{enumerate}[label=(\alph*)]
\item The image $\varphi(G)$ of a homomorphism $\varphi : G \rightarrow H$ is a subgroup of $H$.
\item A set $H$ is a normal subgroup of a group $G$ if and only if it is the kernel of some homomorphism $\varphi : G \rightarrow G^\prime$.
\end{enumerate}
\end{theorem}

If $G$ is a group and $g \in G$, then $Z(g) = \{ h \in G \mid gh = hg \}$ is called the **centralizer** of $g$ and consists of all elements which commute with $g$. The **centre** of a group $G$ is the set $Z(G) = \{ h \in G \mid gh=hg \text{ for all } g \in G \}$ of elements which commute with all other elements of the group. By definition, $Z(G) = \bigcap_{g \in G} Z(g)$ and if $G$ is abelian, then $Z(G) = Z(g) = G$ for every $g \in G$.

If $g \in G$ and $G$ is a group, then the set $\langle g \rangle = \{ g^n \mid n \in \mathbb{Z} \}$ is a subgroup of $G$ and is called the group *generated by* $g$. Conversely, if $G = \langle g \rangle$ for some $g \in G$, we say that $G$ is a **cyclic group**. More generally, for $g_1, g_2, \ldots, g_n \in G$ we can define $\langle g_1, g_2, \ldots, g_n \rangle$ as the subgroup of $G$ consisting of all products of elements $g_i$ and we say that it is generated by the set $\{g_i\}$. We call $|G|$, the number of elements of $G$, the **order** of $G$ and $\mathrm{ord}(g) := |\langle g \rangle|$ the order of $g$; both of these quantities may be finite or infinite. We have the following important fact:
\begin{theorem}
For $g \in G$, if $\mathrm{ord}(g)$ is finite, it is the smallest positive integer $n$ for which $g^n = e$.
\end{theorem}

### Examples of groups {#group-examples}

We now discuss several examples of groups, some of which will be very relevant in the rest of this thesis:

- The integers $\mathbb{Z}$ are a group under addition.

- The integers modulo $n$, $\mathbb{Z}/n\mathbb{Z}$, are a group under addition modulo $n$.

- Every field $\mathbb{F}$ is a group under addition; also, $\mathbb{F}^\times := \mathbb{F} \setminus \{0\}$ is a group under multiplication.

- If $p$ is prime, then $\mathbb{F}_p := \mathbb{Z}/p\mathbb{Z}$ is a field, so $\mathbb{F}_p$ and $\mathbb{F}_p^\times$ are also groups under addition and multiplication modulo $n$.

- Every vector space forms a group under addition; this includes, for example, the vector space of $n \times m$ matrices over a field $\mathbb{F}$, $M_{nm}(\mathbb{F})$.

- The set $GL(n, \mathbb{F}) \subset M_{nn}(\mathbb{F})$ of all invertible $n\times n$ matrices over $\mathbb{F}$ forms a group under matrix multiplication; unlike the previous examples, this is a non-abelian group for $n > 1$.

- The map $\det : GL(n, \mathbb{F}) \rightarrow \mathbb{F}^\times$ is a homomorphism since $\det(AB) = \det(A)\det(B)$; therefore, $SL(n, \mathbb{F}) := \mathrm{ker}(\det)$ is a normal subgroup of $GL(n, \mathbb{F})$ and consists of all matrices with determinant $1$.

Let us now examine these last two examples in some more detail. We can describe the centre of $GL(n, \mathbb{F})$:

\begin{theorem}
$Z(GL(n, \mathbb{F})) = \{\lambda I \mid \lambda \in \mathbb{F} \}$.
\end{theorem}
\begin{proof}
Assume that $A \in Z(GL(n, \mathbb{F}))$. For all $1 \leq i \neq j \leq n$ we define $E^{ij}$ as the matrix with a $1$ as the $(i, j)$ entry and $0$ everywhere else. Since $\det(I + E^{ij}) = 1$, $I + E^{ij} \in GL(n, \mathbb{F})$ and therefore $(I + E^{ij})A = A(I + E^{ij}) \iff E^{ij}A = AE^{ij}$.

From this we get
$$
(E^{ij}A)_{ij} = \sum_{k=0}^n E^{ij}_{ik} A_{kj} = A_{jj} = (AE^{ij})_{ij} = \sum_{k=0}^n A_{ik} E^{ij}_{kj} = A_{ii}
$$
as well as
$$
(E^{ij}A)_{ii} = \sum_{k=0}^n E^{ij}_{ik} A_{ki} = A_{ji} = (AE^{ij})_{ii} = \sum_{k=0}^n A_{ik} E^{ij}_{ki} = 0.
$$
Since this holds for all $i \neq j$, we have $Z(GL(n, \mathbb{F})) \subset \{\lambda I \mid \lambda \in \mathbb{F}\}$.

The converse inclusion is easily shown by noting that $(\lambda I) A = \lambda A = A(\lambda I)$ for all ${A \in GL(n, \mathbb{F})}$.
\end{proof}
\begin{cor}
$Z(SL(2, \mathbb{F})) = \pm I$.
\end{cor}
\begin{proof}
If $A = \lambda I$, then we require that $\det(A) = \lambda^2 = 1$, but in any field, the only two square roots of $1$ are $\pm 1$.
\end{proof}

We will also later need to know the order of $SL(2, \mathbb{F}_p)$ for some prime $p$. While the following results can be proven for arbitrary-size matrices with the same technique, we will focus here on this particular case:

\pagebreak
\begin{theorem}
$|GL(2, \mathbb{F}_p)| = p(p-1)(p^2-1)$.
\end{theorem}
\begin{proof}
For the first row of an invertible matrix $A$, we may choose any pair of entries except for the case that both of them are zero, therefore there are $p^2-1$ possibilities. For the second row, we may again choose any pair of entries except for the $p$ multiples of the first row (which includes the zero row), leading to $p^2 -p$ possibilities. Thus,
$$
|GL(2, \mathbb{F}_p)| = (p^2-1)(p^2-p) = p(p-1)(p^2-1).
$$
\end{proof}
\begin{theorem}\label{slfp-order}
$|SL(2, \mathbb{F}_p)| = p^3-p$.
\end{theorem}
\begin{proof}
Let $n \in \mathbb{F}_p\setminus \{0\}$ and define $GL_n = \{ A \in GL(2, \mathbb{F}_p) \mid \det(A) = n \}$. The map ${\varphi_n : SL(2, \mathbb{F}_p) \rightarrow GL_n}$ defined by $\varphi_n(A) = \begin{pmatrix}n & 0\\0 & 1\end{pmatrix}A$ is a well-defined bijection. Therefore, $|GL_n| = |SL(2, \mathbb{F}_p)|$ for every $n$. It follows that
\begin{align*}
|GL(2, \mathbb{F}_p)| &= \left| \bigcup_{n=1}^{p-1} GL_n \right| = \sum_{n=1}^{p-1} |GL_n| = (p-1)|SL(2, \mathbb{F}_p)|\\
\iff\quad |SL(2, \mathbb{F}_p)| &= \frac{p(p-1)(p^2-1)}{p-1} = p(p^2-1) = p^3-p.
\end{align*}
\end{proof}

If we are given a group $G$, we can form some additional groups out of it:

- The set of bijections $G \rightarrow G$ forms a group under function composition, the **symmetric group** $S_G$; in fact, the group $S_X$ can be defined for all sets $X$, not just groups.

- We can define $Aut(G) \subset S_G$ as the set of all automorphisms of $G$; one can verify that it is closed under composition and inverses, so it is a subgroup of $S_G$.

- Define $Inn : G \rightarrow Aut(G)$ as $Inn(g) = h \mapsto ghg^{-1}$. Theorem \ref{inn-homo} below shows that $Inn$ is well-defined and a homomorphism. By theorem \ref{homo-subgroup} its image $Inn(G)$ is a subgroup of $Aut(G)$; we call it the **inner automorphism group** of $G$ and every $Inn(g)$ is called an inner automorphism.

\pagebreak

\begin{theorem}\label{inn-homo}
$Inn$ is well-defined and a homomorphism.
\end{theorem}
\begin{proof}
For every $g \in G$, $Inn(g)$ is clearly a map from $G$ to itself. As an inverse map is given by $Inn(g^{-1})$, it is a bijection. For $h_1, h_2 \in G$, we have
$$
Inn(g)(h_1h_2) = gh_1h_2g^{-1} = gh_1g^{-1}gh_2g^{-1} = Inn(g)(h_1)Inn(g)(h_2),
$$
so $Inn(g) \in Aut(G)$. Moreover, for every $g_1, g_2, h \in G$,
$$
Inn(g_1g_2)(h) = g_1g_2hg_2^{-1}g_1^{-1} = Inn(g_1)(Inn(g_2)(h)) = (Inn(g_1) \circ Inn(g_2))(h),
$$
so $Inn$ is a homomorphism.
\end{proof}

Finally, if we are given two groups $G, H$, we can form their **direct product** $G \times H := \{ (g, h) \mid g \in G, h \in H \}$. As is easy to verify, this is again a group under the operation $(g_1, h_1)(g_2, h_2) := (g_1g_2, h_1h_2)$.

### Groups acting on a set

We now introduce the central concept of a group acting on a set:
\begin{defn}
Let $G$ be a group and $X$ a set. A homomorphism $\varphi : G \rightarrow S_X$ is called a \textbf{group action} and we say that $G$ is \emph{acting on} $X$ by way of $\varphi$.
\end{defn}

If we don't particularly want to emphasise the homomorphism $\varphi$, we will often write $g(x)$ instead of $\varphi(g)(x)$. In that sense, we can identify $g$ with a bijection on $X$ that respects the group structure, i.e. $(g_1g_2)(x) = g_1(g_2(x))$.

The simplest example of a group action is given by $g(x) = x$ for all $x \in X$. Another example is given by matrix-vector multiplication: $A(x) := Ax$ is an action of $GL(n, \mathbb{F})$ on $\mathbb{F}^n$ (in fact, $A(\cdot) \in Aut(\mathbb{F}^n)$ for every $A$). Finally, $Inn$ also defines a group action of some group $G$ on itself.

### The semi-direct product
We will now introduce the semi-direct product of group, which is a generalisation of the direct product. In order to motivate this definition, we will first discuss the construction by way of an example:

An **invertible affine transformation** is defined as a function $f : \mathbb{R}^n \rightarrow \mathbb{R}^n$ given by
$$
f(x) = Ax + b
$$
for a given matrix $A \in GL(n, \mathbb{R})$ and a vector $b \in \mathbb{R}^n$.

We now claim that the set of all invertible affine transformations on $\mathbb{R}^n$ form a group under composition. Let $f, g$ be invertible affine transformations defined by $f(x)=Ax+b$, $g(x)=Cx+d$. Then we have
$$
(f\circ g)(x) = f(Cx+d) = A(Cx+d)+b = ACx + (Ad + b)
$$
which is also an invertible affine transformation, since $AC$ is invertible as a product of invertible matrices. The identity function $f(x) = Ix + 0$ is an invertible affine transformation and acts as an identity under composition, while the inverse of $f(x) = Ax + b$ is given by $f^{-1}(x) := A^{-1}x-A^{-1}b$, since
$$
(f^{-1}\circ f)(x) = f^{-1}(Ax+b) = A^{-1}(Ax+b)-A^{-1}b = x.
$$
Finally, composition is clearly an associative operation on any set of functions.

Since an invertible affine transformation is uniquely identified by the matrix $A$ and the vector $b$, we can define a bijection between the set of invertible affine transformations and the set $\mathbb{R}^n \times GL(n, \mathbb{R})$.[^order-affine] We can turn this bijection into a group isomorphism by defining an operation on $\mathbb{R}^n \times GL(n, \mathbb{R})$ such that it agrees with the composition of functions. Considering the computation above, this means that we can define
$$
(b, A) \cdot (d, C) = (b+Ad, AC)
$$
as the group operation. This looks similar to the direct product, except that the vector $d$ is being multiplied by $A$. The map $\varphi$ given by $\varphi(A)(x) = Ax$ defines a homomorphism from $GL(n, \mathbb{R})$ to $Aut(\mathbb{R}^n)$, as is easy to check. This leads us directly to the more general definition:

[^order-affine]: It might seem more natural to instead consider the set $GL(n, \mathbb{R}) \times \mathbb{R}^n$. However, doing it this way aligns more closely with the way the semi-direct product is traditionally defined.

\begin{defn}
Let $G, H$ be groups and let $\theta : H \rightarrow Aut(G)$ be a homomorphism. Then, the \textbf{semi-direct product} $G \times_\theta H$ is defined as the set $G \times H$ together with the operation
$$
(g,h)\cdot(g^\prime, h^\prime) = (g\cdot\theta(h)(g^\prime), hh^\prime).
$$
\end{defn}

\begin{remark}
The direct product is a special case of the semi-direct product for which the homomorphism is given by $\theta(h) = \mathrm{id}$.
\end{remark}

\begin{theorem}
The semi-direct product of two groups $G\times_\theta H$ is itself a group.
\end{theorem}
\begin{proof}
Clearly, multiplying two elements of the group yields another element of the set $G\times H$. Multiplication is associative, since
\begin{align*}
((g,h)\cdot(g^\prime,h^\prime))\cdot(g^{\prime\prime},h^{\prime\prime}) &= (g\theta(h)(g^\prime),hh^\prime)\cdot(g^{\prime\prime},h^{\prime\prime}) \\
&= (g\theta(h)(g^\prime)\theta(hh^\prime)(g^{\prime\prime}), hh^\prime h^{\prime\prime})\\
&= (g\theta(h)(g^\prime)\theta(h)(\theta(h^\prime)(g^{\prime\prime})), hh^\prime h^{\prime\prime})\\
&= (g\theta(h)(g^\prime\theta(h^\prime)(g^{\prime\prime})), hh^\prime h^{\prime\prime})\\
&= (g, h)\cdot(g^\prime\theta(h^\prime)(g^{\prime\prime}),h^\prime h^{\prime\prime})\\
&= (g,h)\cdot ((g^\prime,h^\prime), (g^{\prime\prime},h^{\prime\prime})
\end{align*}
where we're using the fact that both $\theta$ and $\theta(h)$ for any $h \in H$ are homomorphisms.

Since $\theta$ is a homomorphism, $\theta(e_H) = \mathrm{id}$, so we also have
$$
(e_G, e_H)\cdot(g, h) = (g, h)
$$
for any $g \in G, h \in H$, which gives us our identity element.

We now claim that if $(g,h)$ is an element of $G \times_\theta H$, its inverse element is given by $(\theta(h^{-1})(g^{-1}), h^{-1})$. This is shown by
$$
(g,h)\cdot(\theta(h^{-1})(g^{-1}),h^{-1}) = (g\theta(h)(\theta(h)^{-1}(g^{-1})), hh^{-1}) = (gg^{-1},hh^{-1}) = (e_G, e_H).
$$
\end{proof}

\begin{theorem}\label{semidirect-subgroup}
$G$ is isomorphic to a normal subgroup of $G \times_\theta H$.
\end{theorem}
\begin{proof}
Let us define $G^\ast := \{(g, e) \mid g \in G\} \subset G \times_\theta H$. The map $\phi = g \mapsto (g, e_H)$ is a bijection between $G$ and $G^\ast$. Since
$$
\phi(gh) = (gh, e) = (g, e)(h, e) = \phi(g)\phi(h),
$$
it is a group isomorphism.

We show that $G^\ast$ is a normal subgroup of $G \times_\theta H$. If $(g, e), (h, e)$ are elements of $G^\ast$, then $(g, e)(h, e)^{-1} = (g, e)(h^{-1}, e) = (gh^{-1}, e) \in G^\ast$, which shows that $G^\ast$ is a subgroup.

Let us now consider $(x, y) \in G \times_\theta H$ and $(g, e) \in G^\ast$. It is not hard to see that the second component of $(x,y)^{-1}$ is $y^{-1}$ and that the second component of a product in $G \times_\theta H$ is always equal to the product (in $H$) of the second components. Therefore, the second component of $(x,y)(g,e)(x,y)^{-1}$ is $yey^{-1} = e$ and therefore $G^\ast$ is a normal subgroup.
\end{proof}
In light of the previous theorem, we will henceforth identify $G$ directly as a normal subgroup of $G \times_\theta H$.

### Computational problems {#group-problems}

We will now discuss some computational problems in group theory which will be relevant for the security of the encryption scheme discussed in this thesis.

\begin{defn}
Let $G$ be a group.
\begin{enumerate}[label=(\alph*)]
\item If we are given $x, y \in G$, the \textbf{conjugacy problem} (CP) consists of finding an element $g \in G$ such that $gxg^{-1} = y$ (or determining that no such element exists). The pair $(x, y)$ is called an \emph{instance} of the conjugacy problem.
\item If we are given some inner automorphism $\varphi$, the \textbf{special conjugacy problem} (SCP) consists of finding some $g$ such that $Inn(g) = \varphi$.
\end{enumerate}
\end{defn}

Depending on the circumstances, we may in both cases also be interested in finding the full solution set to the problem instead of just one particular solution. It's always possible to solve both of these problems by inspecting every element of the group. However, whether there exists a more efficient method of solving the problem will depend on the particular group. Nevertheless, we can describe the structure of the solution sets for both these problems:

\begin{theorem}\label{solving-scp}
\leavevmode
\begin{enumerate}[label=(\alph*)]
\item Let $(x, y)$ be an instance of the CP and let $g$ be a solution. Then, the solution set of this instance is $g Z(x)$.
\item Let $g$ be a solution of the SCP. Then, the solution set is $g Z(G)$.
\end{enumerate}
\end{theorem}
\begin{proof}
\leavevmode
\begin{enumerate}[label=(\alph*)]
\item For any $h \in G$ let $z = g^{-1}h$. Then,
$$
hxh^{-1} = gxg^{-1} \iff zxz^{-1} = x \iff zx = xz \iff z \in Z(x).
$$
This means that $h$ is a solution if and only if $h = gz$ for some $z \in Z(x)$.
\item For any $h \in G$, write $S_h$ for the solution set of the CP instance $(h, \varphi(h))$. Since $Inn(g) = \varphi$, we have that $S_h = gZ(h)$. The solutions of the SCP are exactly the elements that solve all CP instances, and therefore the solution set $S$ of the SCP is given by
$$
S = \bigcap_{h \in G} S_h = \bigcap_{h \in G} gZ(h) = g\bigcap_{h \in G}Z(h) = gZ(G).
$$
\end{enumerate}
\end{proof}

Therefore we see that we may solve the SCP by applying the given inner automorphism to every element of the group and solving the respective CP instance. In section \ref{scp-in-slfp}, we'll see an example of a group where it suffices to solve the CP for just two instances.

\begin{cor}
$Inn(g)$ is the identity map if and only if $g \in Z(G)$
\end{cor}
\begin{proof}
The identity element $e$ is a solution for the SCP described by the identity map and so the full solution set is given by $eZ(G) = Z(G)$.
\end{proof}

Another computational problem that will be relevant for us is the following:

\begin{defn}
Let $G$ be a cyclic group of order $n$. If we are given a generator $g$ and some $h \in G$, then the \textbf{discrete logarithm problem} (DLP) consists of finding an $x \in \{0, \ldots, n-1\}$ such that $g^x = h$.
\end{defn}

While DLP is only defined for cyclic groups with a generator $g$ (and all practical algorithms rely on these assumptions), it can also be applied to more general situations. If $G$ is any group and $g \in G$, then we may try to solve DLP in the subgroup $\langle g \rangle$, although it may be necessary to first verify whether $h \in \langle g \rangle$. A simple brute-force algorithm that just tries all $\mathrm{ord}(g)$ possible values will always succeed, so for DLP to be difficult to solve, a necessary condition is that $\mathrm{ord}(g)$ (and, by extension, $|G|$) be large.

The difficulty of DLP crucially depends on the particular group under consideration. For example, it is easy to solve DLP in the additive group $\mathbb{Z}/n\mathbb{Z}$ since doing so only requires computing multiplicative inverses in the ring $\mathbb{Z}/n\mathbb{Z}$, which can be done efficiently. However, the fastest algorithms that can solve DLP for arbitrary groups run in time roughly $\mathcal{O}(\sqrt{p})$, where $p$ is the largest prime factor of the group order $n$ \cite[pp.~348--354]{crypto_book}. While this is faster than the brute-force algorithm, which requires $\mathcal{O}(n)$ steps, it's still asymptotically inefficient in terms of the bit-size: if $k \approx \log_2(p)$ is the bit-size of $p$, then these algorithms will run in time $\mathcal{O}(2^{k/2})$, i.e. in exponential time. A faster algorithm, the *index calculus*, exists in the case of subgroups of $\mathbb{F}_p^\times$ and runs in time roughly $\mathcal{O}(2^{k^{1/3}})$, which is sub-exponential and therefore faster than the generic algorithms (although still not polynomial) \cite[pp.~354--356]{crypto_book}. However, it can't be generalised to arbitrary groups.
