# The MOR cryptosystem {#mor}
In this section, we will discuss a public-key cryptosystem, the MOR cryptosystem, that was first introduced by Paeng et al. in \cite{mor_paper}. However, we first discuss a more general scheme that unifies both the MOR cryptosystem and the familiar El-Gamal encryption scheme.[^independent] The correctness of the system can be proved for this general case, but its security and efficiency will generally depend on the specific instance. In section \ref{mor-inner}, we will apply this general scheme to inner automorphism groups, which will result in the MOR cryptosystem. This cryptosystem still offers some choice in terms of the underlying group that is used. We will therefore only be able to offer some general remarks about its security and efficiency.

[^independent]: A similar generalisation was also discovered in \cite{mor_followup}.

## A generalised El-Gamal encryption scheme
Let $G$ be a group acting on a set $X$ by means of a group action $\varphi$ and $g \in G$. We now define the following asymmetric encryption scheme with plaintext and ciphertext spaces both equal to $X$:

\begin{scheme}[Generalised El-Gamal encryption scheme]
Bob chooses some element $a \in \mathbb{Z}$ as a \emph{private key}. The corresponding \emph{public key} will be the pair $(\varphi(g), \varphi(g^a))$.

Alice can now send an encrypted message $m \in X$ to Bob using the following steps:

\begin{enumerate}
\item Alice chooses some $b \in \mathbb{Z}$ and computes $\kappa := \varphi(g^a)^b$ as well as $\theta := \varphi(g)^b$.
\item She computes $E = \kappa(m)$.
\item She sends the pair $(E, \theta)$ to Bob.
\end{enumerate}

Bob can then retrieve the original message in the following way:

\begin{enumerate}
\item Bob computes $\theta^{-a}$.
\item He computes $\theta^{-a}(E)$, which will be equal to the original message.
\end{enumerate}
\end{scheme}

Notice that encryption is not deterministic if $b$ is chosen at random. We now claim:
\pagebreak
\begin{theorem}
The generalised El-Gamal encryption scheme is correct.
\end{theorem}
\begin{proof}
We must show that, for every message $m \in X$, $\theta^{-a}(\kappa(m)) = m$. This easily follows from the fact that $\varphi$ is a group homomorphism, since
\begin{align*}
\theta^{-a}(\kappa(m)) &= \left(\varphi(g)^b\right)^{-a}(\varphi(g^a)^b(m))\\
&= \varphi(g^{-ab})(\varphi(g^{ab})(m))\\
&= \varphi(g^{ab})^{-1}(\varphi(g^{ab})(m))\\
&= m.
\end{align*}
\end{proof}

While this shows that the system works correctly, whether it is efficient and secure will depend crucially on the choice of $G$, $X$, and $\varphi$. However, we can make some general remarks:

First, it must be possible to encode arbitrary plaintexts as elements of $X$ and any such encoding (and decoding) needs to be computed efficiently. Similarly, it must be efficient to represent permutations of $X$ and the required group operations in the corresponding permutation group (such as exponentiation and computing inverses) have to be carried out efficiently.

Second, it must not be possible to efficiently compute the private key $a$ from $\varphi(g)$ and $\varphi(g^a) = \varphi(g)^a$. This means that DLP needs to be hard in $\langle \varphi(g) \rangle < S_X$.

## The El-Gamal cipher {#el-gamal}

If $G$ is a cyclic group such that $G=X=\langle g \rangle$ and $\varphi(g) := h \mapsto gh$, we get the well-known El-Gamal encryption scheme: The public key is $(g, g^a)$, Alice sends $(g^{ab}m, g^b)$ to Bob and Bob can compute $g^{-ab}g^{ab}m=m$. We will now briefly discuss why El-Gamal is considered to be a secure encryption scheme for the right choice of $G$ (see \cite[pp.~400--404]{crypto_book} for a more thorough discussion):

Let us assume that the parameters $a$ and $b$ are both chosen uniformly at random from the set $\{0, \ldots, \mathrm{ord}(G)-1\}$. One easily shows that the value of $g^{ab}$ will also be distributed uniformly among the elements of $G$, since $g$ is a generator. For every plaintext $m$ and every ciphertext $c$, we have $P(C=c\mid M=m) = P(g^{ab}=cm^{-1}) = k$ for some constant $k$. Therefore, if we are given a ciphertext $c$, the probability that the plaintext is some particular $m_0$ is

\begin{align*}
P(M=m_0\mid C=c) &= \frac{P(C=c\mid M=m_0)P(M=m_0)}{\sum_{m \in G} P(C=c\mid M=m)P(M=m)}\\
&= \frac{kP(M=m_0)}{k\sum_{m \in G}P(M=m)}\\
&= P(M=m_0),
\end{align*}
in other words, if we know just the ciphertext but ignore the public key, we cannot get any information about the plaintext.[^perfect] In order to break the scheme, we therefore have to use information from the public key. In particular, one can show that it is necessary to compute $g^{ab}$ from $g^a$ and $g^b$ (the so-called *Diffie-Hellman problem*). This problem is believed to be hard for many groups, e.g. for prime-order subgroups of $\mathbb{F}_p^\times$ or for elliptic curves \mbox{\cite[pp.~319--332]{crypto_book}}. On the other hand, if we can show that DLP is easy in $G$, then the system is immediately broken because we can compute $a$ from $g^a$.

[^perfect]: This looks identical to the notion of perfect secrecy for symmetric ciphers, but recall that we weren't able to define an analogous (and useful) notion for public-key systems, since we must assume that the public key is, in fact, known.

We must note that when using the El-Gamal encryption scheme, it is of crucial importance to choose a different exponent $b$ for every encryption, otherwise the scheme is vulnerable to a chosen-plaintext attack as follows: Suppose the attacker can get an encryption for some message $m_0$ that they chose. Then they know $m_0$ and $g^{ab}m_0$. If Alice now encrypts a message $m_1$ with the same exponent $b$, the attacker can intercept $g^{ab}m_1$ and compute $m_0\left(g^{ab}m_0\right)^{-1}g^{ab}m_1 = m_0m_0^{-1}g^{-ab}g^{ab}m_1 = m_1$ and the plaintext message is exposed.

## Application to inner automorphism groups {#mor-inner}

We now turn our attention towards the **MOR cryptosystem** as discussed in \cite{mor_paper, mor_followup}, which is an application of the generalised El-Gamal cipher.

### Description of the MOR cryptosystem

We will again choose some group $G$, but the set $X$ upon which it acts will be a normal subgroup $H$ of $G$.[^1] The group action will be the inner automorphism map $Inn$, so an element $g$ will act on an element $h$ by way of conjugation, i.e. $g(h) = ghg^{-1}$. Since normal subgroups are closed under conjugation and since every inner automorphism of $G$ is an element of $Aut(G)$, $Inn(g)|_{H}$ is an element of $Aut(H) \subset S_H$ for every $g \in G$. By theorem \ref{inn-homo}, we know that $Inn$ (or more precisely, the map $g \mapsto Inn(g)|_H$) is a homomorphism and therefore, in fact, a group action on $H$.

We will also now start to consider in some more detail how the encryption scheme can actually be carried out in practice. To that end, we require that there be a (small enough) set of generators $S$ of $H$, and that for every element $h \in H$ its decomposition into ${h = \prod_{s \in S}s^{k_s}}$ can be computed efficiently.[^decomp] In such a case, we have that
$$
Inn(g)(h) = \prod_{s \in S} \left(Inn(g)(s)\right)^{k_s}
$$
for every $g \in G$, so all the information about $Inn(g)|_{H}$ can be recovered from the set $\{ Inn(g)(s) \mid s \in S \}$. This makes it easy to encode $Inn(g)$, as long as elements of $H$ are easy to encode.

[^decomp]: If $H$ is non-abelian, a single element $s \in S$ may actually appear multiple times in the product with different exponents. We will nevertheless use this simplified notation $h = \prod_{s\in S} s^{k_s}$ by treating repeated occurences of an element $s$ as distinct elements.

We can therefore describe MOR as follows:
\begin{scheme}[MOR cryptosystem]
Let $G$ be a group and $g$ be a fixed element of $G$. Let $H$ be a normal subgroup of $G$ with a set of generators $S$.

Bob chooses some element $a \in \mathbb{Z}$ as a \emph{private key}. The corresponding \emph{public key} will be the pair $(Inn(g), Inn(g^a))$, i.e the sets $\{ Inn(g)(s) \mid s \in S \}$ and $\{ Inn(g^a)(s) \mid s \in S \}$.

Alice can now send an encrypted message $m \in H$ to Bob using the following steps:

\begin{enumerate}
\item Alice chooses some $b \in \mathbb{Z}$ and computes $\theta := Inn(g^b)$, i.e. $\{ Inn(g)^b(s) \mid s \in S \}$.
\item She computes the decomposition $m = \prod_{s\in S} s^{k_s}$ and the encryption $E = Inn(g^{ab})(m) = \prod_{s \in S} \left(Inn(g^a)^b(s)\right)^{k_s}$
\item She sends the pair $(E, \theta)$ to Bob.
\end{enumerate}

Bob can then retrieve the original message in the following way:

\begin{enumerate}
\item Bob computes $\theta^{-a}$, i.e. $\{ \theta^{-a}(s) \mid s \in S \}$.
\item He computes the decomposition $E = \prod_{s\in S} s^{l_s}$ and $\theta^{-a}(E) = \prod_{s\in S} \left(\theta^{-a}(s)\right)^{l_s}$, which will be equal to the original message.
\end{enumerate}
\end{scheme}

Since this scheme is just a particular instance of the generalised El-Gamal scheme, we have already proven its correctness. We have to show that the computations in the algorithm can be carried out in practice. In order for Alice to compute e.g. $Inn(g)^b(s)$ from $Inn(g)(s)$, she can first decompose $Inn(g)(s)$ into another product of generators, then apply $Inn(g)$ again on each generator and multiply the results, repeating these steps $b$ times in total---this can be made even more efficient by the use of repeated squaring. We will discuss this in more detail in section \ref{efficiency} in the case of a concrete example. The same principle applies to computing $Inn(g^a)^b(s)$ given $Inn(g^a)(s)$.

For Bob to be able to compute the inverse $\theta^{-a}$, we will at this point assume that the SCP is easy to solve in $G$. If he can find some $\overline{g}$ for which $Inn(\overline{g}) = \theta$, then, since $Inn$ is a homomorphism, ${Inn(\overline{g}^{-1}) = Inn(\overline{g})^{-1} = \theta^{-1}}$. Bob can then compute $\theta^{-a}$ in the same way that Alice carried out her computations. Alternatively, Alice could also send $b$ instead of $\theta$---depending on the circumstances, it might be easier for Bob to compute $Inn(g^{-ab})$ that way. At any rate, the efficiency of encryption and decryption in this system will again depend on the particular group in question.

### Security of the MOR cryptosystem {#mor-security}

We will now consider some arguments related to the security of the MOR cryptosystem.

First, we note that, unlike the El-Gamal scheme, ciphertexts of MOR do leak information about the plaintext; in particular, we know that the plaintext and ciphertext have to be in the same conjugacy class. It may be possible to mitigate this problem by a padding scheme such as the one we discuss in section \ref{slfp-rep}. One would have to show that this scheme distributes plaintexts uniformly onto the different conjugacy classes, something which we will not attempt to do here (nor was this done by Paeng et al.).

Next, we'll determine some necessary (though not sufficient) conditions for MOR to be secure:

- $G$ can obviously not be abelian, because otherwise $ghg^{-1} = h$ for all plaintexts $h$.

- More in general, for any choice of $g$ and $h$, the two elements should not commute (and in particular, neither $g$ nor any element of $H$ may be in the centre of $G$). If we use padding, we can detect whether $g$ and $h$ commute during encryption and try again with a different choice of padding.

- The security of the system crucially depends on the difficulty of solving DLP in $Inn(G)$, or more precisely, in $\langle g \rangle$.

Finally, we'll discuss the rationale that Paeng et al. provide for the security of MOR. Unfortunately, as pointed out in \cite{mor_analysis}, they do not provide any formal proofs of security (similar to what we discussed in section \ref{pubkey-security}), neither for the general scheme nor for their particular choice of group (which we will discuss in sections \ref{slfp} and \ref{security}). They do however give some arguments for why they believe their system could prove secure, at least given a suitable choice of $G$.

As Paeng et al. note, it is difficult to find non-abelian groups $G$ for which the SCP is hard to solve. If DLP in $G$ is also easy, this could lead to an attack along the following lines:

\begin{enumerate}
\item The attacker can find the set $S$ of all $g_0$ with $Inn(g_0) = Inn(g^a)$.
\item They can try to find $g^a$ among $S$.
\item Since DLP is easy in $G$ and $g$ is known, they can compute the private key $a$.
\end{enumerate}

Paeng et al.'s suggestion is to make step 2 difficult. From theorem \ref{solving-scp} we know that $S$ contains $|Z(G)|$ elements. If there aren't many elements in the centre, then an attacker can easily try finding the value of $a$ by solving DLP for all elements of $S$. If the plaintext is some meaningful message---or at least used to encrypt some meaningful message---it will be easy to determine which value of $a$ is the correct key. If, on the other hand, there are a lot of elements in the centre, then it may be computationally infeasible to solve DLP for all possible choices. Thus we can describe one additional necessary condition for the security of the scheme:

- If both DLP and SCP are easy in $G$, then $Z(G)$ needs to be large.

As for solving DLP in $Inn(G)$ via other methods, Paeng et al. point out that the fastest-known method, the index calculus, isn't applicable for inner automorphism groups and that the more general methods are too slow (see section \ref{group-problems}).

It is important to emphasise that this informal reasoning doesn't prove the system to be secure in general, nor does it provide us with methods to decide whether it is secure given a concrete choice of $G$. On the one hand, it is not shown that breaking the system *requires* solving DLP in $Inn(G)$. On the other hand, it is entirely conceivable that it could be easy to solve DLP for a particular $Inn(G)$ even when its centre is large. That general methods of solving DLP are too slow doesn't show that there can't exist, for a particular group $G$, algorithms that can solve DLP in $Inn(G)$ efficiently. The conditions we have listed so far are thus *necessary* but not *sufficient* conditions for MOR to be secure. Indeed, section \ref{attacks} will show how easy it can be to break MOR for one particular choice of $G$.

### Reuse of exponents {#modified-mor}

We have seen in the case of the El-Gamal cryptosystem how crucial it is that the exponent $b$ be chosen anew for each new encryption, since otherwise the encryption can easily be broken by computing $\left(g^{ab}m_0\right)^{-1}g^{ab}m_1 = m_0^{-1}m_1$. In \cite{mor_paper}, Paeng et al. argue that the same attack can't be repeated for MOR. Indeed, we can compute

$$
\left(Inn(g^{ab})(m_0)\right)^{-1}Inn(g^{ab})(m_1) = Inn(g^{ab})(m_0^{-1})Inn(g^{ab})(m_1) = Inn(g^{ab})(m_0^{-1}m_1),
$$

but it's not clear how we could compute $m_0^{-1}m_1$ without knowing $g^{ab}$. Paeng et al. argue that, due to this fact, the exponent $b$ can be fixed. This allows Alice and Bob to precompute the values of $Inn(g^{ab})$ and $Inn(g^{-ab})$, respectively, which can speed up encryption and decryption considerably (we will discuss the efficiency of MOR in the context of one particular group in section \ref{efficiency}).

Paeng et al. are not very explicit about what they mean when they say that $b$ can be fixed. Clearly, $b$ can't be fixed for everyone who wants to send an encrypted message to Bob: Otherwise, $b$ would be part of the public key and an attacker would be able to compute $Inn(g^a)^b = Inn(g^{ab})$. In order to break the system, all an attacker would have to do is to invert $Inn(g^{ab})$, something which is necessarily easy to do (otherwise, Bob would have no chance at decryption).

It seems that Paeng et al. are rather suggesting that $g$ be fixed for every *pair* of participants. Thus, the first time Alice and Bob communicate, Alice chooses a parameter $b$, computes $Inn(g^{ab})$ and $Inn(g^b)$ and sends the latter to Bob, who will use it to precompute $Inn(g^{-ab})$. From this moment on, Alice will simply encrypt every message by applying $Inn(g^{ab})$ to the plaintext, while Bob will apply $Inn(g^{-ab})$ to the ciphertext.

It is not entirely clear how much of a benefit this precomputation really entails. As we have pointed out in section \ref{pubkey}, public key cryptosystems are generally so slow that they're most commonly used in combination with a symmetric cipher which will be used to encrypt the actual messages. Therefore, it is unlikely that Alice and Bob would want to exchange a large number of messages using MOR which means that precomputing certain values might not be particularly useful. Moreover, we will see in section \ref{break-reuse} that, given the particular choices made by Paeng et al., the cryptosystem is easily broken if the exponent $b$ is reused.

[^1]: In \cite{mor_paper}, Paeng et al. only consider $H$ acting on itself (and $H$ is, of course, a normal subgroup of itself). However, we will see in section \ref{slfp} that in the case of the concrete group $SL(2, \mathbb{F}_p) \times \mathbb{F}_p$, the plaintext (and ciphertext) messages will consist of elements of $SL(2, \mathbb{F}_p)$, which is a normal subgroup. Therefore, allowing $H$ to act on normal subgroups allows for a slighly more elegant treatment.
