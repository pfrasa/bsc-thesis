\section{Security of the system over \texorpdfstring{$SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$}{SL(2, Fp) x Fp}}\label{security}

Let us now investigate the security of MOR when implemented over ${SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p}$. In sections \ref{scp-in-slfp}--\ref{dlp-in-inn}, we will reconstruct Paeng et al.'s argument for why they believe their system to be secure. We will see that, although SCP is easy to solve in $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$, its centre is large enough that simply trying to solve DLP for every possible solution would prove unfeasible. Nevertheless, in section \ref{attacks} we will see ways by which the system can be successfully attacked.

\subsection{Solving SCP in \texorpdfstring{$SL(2, \mathbb{F}_p)$}{SL(2, Fp)}}\label{scp-in-slfp}
We will first try to solve the special conjugacy problem in $SL(2,\mathbb{F}_p)$. We assume there is a matrix
$$
X = \begin{pmatrix}x & y\\z & w\end{pmatrix}
$$
that we don't know, but that we can compute $XAX^{-1}$ for certain known matrices $A$. Our goal is to find a matrix $X^\prime$ such that $Inn(X) = Inn(X^\prime)$. Since we know that the solution set is $XZ(SL(2,\mathbb{F}_p)) = \pm X$, it follows that the solution is unique up to sign. We will first demonstrate the general technique on a concrete example and then show that, under certain conditions, it will always work.

Consider the two matrices
$$
A_1 = \begin{pmatrix}1 & -2\\0 & 1\end{pmatrix}\quad\quad A_2 = \begin{pmatrix}1 & 0\\-2 & 1\end{pmatrix}
$$
and assume that
$$
B_1 := XA_1X^{-1} = \begin{pmatrix}31 & -18\\50 & -29\end{pmatrix}\quad\quad B_2 := XA_2X^{-1} = \begin{pmatrix}-3 & 2\\-8 & 5\end{pmatrix}.
$$
We compare the $(1,1)$ entries of both sides of $XA_1 = B_1X$ and the $(1,2)$ and $(2, 1)$ entries of $XA_2 = B_2X$ to get the following three equations:
\begin{align*}
x &= 31x - 18z \iff z = \frac{5}{3}x\\
y &= -3y + 2w \iff y = \frac{1}{2}w\\
z-2w &= -8x + 5z \iff w = -2z+4x.
\end{align*}
By plugging in the value of $z$ from the first equation in the last equation, we get $w = \frac{2}{3}x$. Plugging this into the second equation, we get $y = \frac{1}{3}x$. This means that a matrix satisfying the conjugacy equations must have the shape
$$
X^\prime = k\begin{pmatrix}3 & 1\\5 & 2\end{pmatrix}.
$$
Because $\det(X^\prime)=1=k^2$, we know that $k=\pm 1$. Either of these two possibilities must lead to the original matrix $X$ and since the solution set is $\pm X$, it follows that
$$
\pm \begin{pmatrix}3 & 1\\5 & 2\end{pmatrix}
$$
are indeed the two unique solutions.

More in general, we can try to solve $XA_1 = A_1X, XA_2 = A_2X$ for any two given matrices $A_1, A_2$. Following \cite{mor_analysis}, we'll show that such a pair of matrices will always allow us to solve the special conjugacy problem as long as $A_2 \notin Z(A_1)$. First, we'll need the following theorem:
\begin{theorem}\label{two-matrices-suffice}
Let $A, B, U \in SL(2, \mathbb{F}_p)$ such that $U \in Z(A) \cap Z(B)$ and $B \notin Z(A)$. Then, $U \in Z(SL(2, \mathbb{F}_p))$.
\end{theorem}
\begin{proof}
We define $\varphi : M_{22}(\mathbb{F}_p) \rightarrow M_{22}(\mathbb{F}_p)$ as $\varphi(M) = UM-MU$. We immediately verify that $\varphi(\alpha M + \beta N) = \alpha\varphi(M)+\beta\varphi(N)$, so $\varphi$ is an endomorphism. We see that $\{U, A, B, I\} \in \mathrm{ker}(\varphi)$.

If $A = \lambda U$ for $\lambda \in \mathbb{F}_p$, then $AB = \lambda UB = B(\lambda U) = BA$, a contradiction. So, $A \notin \langle U \rangle$. If $B = \lambda U + \mu A$, then $AB = A\lambda U + A\mu A = \lambda UA + \mu A^2 = BA$, another contradiction, and therefore $B \notin \langle U, A \rangle$.

Now assume that $I = \lambda U + \mu A + \nu B$. Let's first consider $\mu = \nu = 0$. Then, $U = \frac{1}{\lambda}I$, so $U$ commutes with every other matrix, as required. Otherwise, assume that $\mu = 0, \nu \neq 0$. In that case, $(\lambda U + \nu B)A = A(\lambda U + \nu B)$, which is equivalent to $BA=AB$. If $\mu\neq 0$, we have $(\lambda U + \mu A + \nu B)B = B(\lambda U + \mu A + \nu B)$, and by cancelling we get again $AB = BA$. These contradictions imply that $I \notin \langle U, A, B \rangle$.

Therefore, the set $\{U, A, B, I\}$ is linearly independent and $\dim(\mathrm{ker}(\varphi)) = 4$, but that means that $UM = MU$ for every matrix $U$, which proves the theorem.
\end{proof}

Let's now assume that we have two instances of the conjugacy problem ${(A_1, B_1 := XA_1X^{-1})}$, $(A_2, B_2 := XA_2X^{-1})$ where $A_2 \notin Z(A_1)$. From $XA_1 = B_1X$, we get a system of four linear equations with four unknowns (the components of $X$). These, together with the condition that $\det(X) = 1$, fully describe the solution set $S_1$ of the first instance. According to theorem \ref{solving-scp}, $S_1 = XZ(A_1)$. We can apply the same procedure to find the solution set $S_2 = XZ(A_2)$ of the second instance. The solution set of the special conjugacy problem must lie in $S_1 \cap S_2$, that is, every solution $X^\prime$ must satisfy $X^\prime = XU$ with some $U \in Z(A_1) \cap Z(A_2)$. From theorem \ref{two-matrices-suffice}, we know that this implies $U \in Z(SL(2, \mathbb{F}_p))$, but then $S_1 \cap S_2 = XZ(SL(2, \mathbb{F}_p)) = \pm X$, that is, we have already found the solution set of the special conjugacy problem.

\subsection{Solving SCP in \texorpdfstring{$SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$}{SL(2, Fp) x Fp}}\label{scp-in-mor}
We now turn to solving the same problem, but in the group $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$. First, the following theorem will help us reduce the problem to the problem of the previous section:

\begin{theorem}\label{inn-equality}
Let $(X, y) \in SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$. Then
$$
\left.Inn((X,y))\right|_{SL(2, \mathbb{F}_p)} = Inn\left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right).
$$
\end{theorem}
\begin{proof}
Recall that we identify $SL(2, \mathbb{F}_p)$ with $\{(M, 0) \mid M \in SL(2,\mathbb{F}_p)\} \vartriangleleft SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$. For any matrix $A \in SL(2, \mathbb{F}_p)$, we have that
\begin{align*}
(X,y)(A, &0)(X, y)^{-1}\\
&= \left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}A\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}, y\right)\left(\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}X^{-1}\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}, -y\right)\\
&= \left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}A\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}X^{-1}\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}, 0\right)\\
&= \left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}A\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}X^{-1}, 0\right)\\
&\cong \left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)A\left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)^{-1}.
\end{align*}
\end{proof}

The next theorem shows how we can describe all solutions to the conjugacy problem:
\begin{theorem}
Let $(X_1, y_1), (X_2, y_2) \in SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$. Then, $Inn((X_1, y_1)) = Inn((X_2, y_2))$ if and only if $X_1\begin{pmatrix}1 & y_1\\0 & 1\end{pmatrix} = \pm X_2\begin{pmatrix}1 & y_2\\0 & 1\end{pmatrix}$.
\end{theorem}
\begin{proof}
For any $(X, y), (A, n) \in SL(2, \mathbb{F}_p)\times_\theta \mathbb{F}_p$, we have
\begin{align*}
(X, y)(A, &n)(X, y)^{-1}\\
&= \left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}A\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}, y+n\right)\left(\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}X^{-1}\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}, -y\right)\\
&= \left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}A\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}\begin{pmatrix}1 & y+n\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}X^{-1}\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -y-n\\0 & 1\end{pmatrix}, n\right)\\
&= \left(\left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)A\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}\left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)^{-1}\begin{pmatrix}1 & -n\\0 & 1\end{pmatrix}, n\right)\\
&= \left(\left(-X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)A\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}\left(-X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)^{-1}\begin{pmatrix}1 & -n\\0 & 1\end{pmatrix}, n\right)
\end{align*}
Therefore $Inn((X, y))$ only depends on $\pm X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}$, which proves one direction of the theorem.

For the other direction, assume that $Inn((X_1, y_1)) = Inn((X_2, y_2))$. By the previous theorem, $Inn\left(X_1\begin{pmatrix}1 & y_1\\0 & 1\end{pmatrix}\right) = Inn\left(X_2\begin{pmatrix}1 & y_2\\0 & 1\end{pmatrix}\right)$. Then, the proof follows from what we've shown in the preceding section.
\end{proof}

This means that, given some $Inn((X,y))$ we can find two matrices $U_{1,2} = \pm X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}$ by applying $Inn((X,y))$ to suitable matrices $A_1, A_2$, as shown in the preceding section. We may then choose any $y^\prime \in \mathbb{F}_p$ and calculate $X^\prime = U_i\begin{pmatrix}1 & -y^\prime\\0 & 1\end{pmatrix}$ for $i \in \{1,2\}$. By the previous theorem $Inn((X^\prime, y^\prime))= Inn(X, y)$. This gives us $2p$ possible solutions. The problem of finding all solutions can thus be solved efficiently.

According to theorem \ref{solving-scp}, $Inn(g_1) = Inn(g_2)$ if and only if $g_2 \in g_1Z(G)$, or equivalently, $g_1^{-1}g_2 \in Z(G)$. This gives us a way to describe the centre of $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$: If we fix some element $(X_1, y_1)$, then for all $(X_2, y_2)$ with $Inn((X_1,y_1)) = Inn((X_2, y_2))$,
\begin{align*}
(X_1,y_1)^{-1}(X_2, y_2) &= \left(\left(X_1\begin{pmatrix}1 & y_1\\0 & 1\end{pmatrix}\right)^{-1}X_2\begin{pmatrix}1 & y_1\\0 & 1\end{pmatrix}, y_2-y_1\right)\\
&= \left(\left(\pm X_2\begin{pmatrix}1 & y_2\\0 & 1\end{pmatrix}\right)^{-1}X_2\begin{pmatrix}1 & y_1\\0 & 1\end{pmatrix}, y_2-y_1\right)\\
&= \left(\pm \begin{pmatrix}1 & y_1 - y_2\\0 & 1\end{pmatrix}, y_2-y_1\right)
\end{align*}
is a central element, and on the other hand, for every central element $z$,  we have that $(X_2, y_2) := (X_1,y_1)z$ satisfies the above equation. Since we may choose $y_2 \in \mathbb{F}_p$ freely, we can write
$$
Z(SL(2,\mathbb{F}_p)\times_\theta \mathbb{F}_p) = \left\{\left(\pm\begin{pmatrix}1 & -y\\0 & 1\end{pmatrix}, y\right) \,\middle|\, y \in \mathbb{F}_p \right\}.
$$
As expected, this has order $2p$.

## Solving DLP in the inner automorphism group {#dlp-in-inn}

We now want to solve DLP in the group $Inn(SL(2,\mathbb{F}_p)\times_\theta \mathbb{F}_p)$. As we discussed in section \ref{mor-security}, if this problem is easy to solve, then the MOR cryptosystem is broken, as we can derive the private key $a$ from the public key $Inn(g)^a$. We also described an algorithm to solve DLP for inner automorphism groups:

1. Find the set $S$ of all $g^\prime$ with $Inn(g^\prime) = Inn(g^a)$.
2. Find $g^a$ among $S$.
3. Given $g$ and $g^a$, find $a$.

We have shown in the preceding sections that step 1 is easy to execute for our particular choice of group. Step 3 involves solving DLP in $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ and also usually poses no problem: If $(X, y)^a = (M, n)$, we can easily see that $n = ay$. If $y \neq 0$, then we can easily solve for $a$ with a single division (if, on the other hand, $y=0$, the problem reduces to DLP in $SL(2, \mathbb{F}_p)$). However, for step 2, we seem to have no other choice than to try all $2p$ solutions of the SCP in turn. This means that the bit complexity of this algorithm is exponential, so it shouldn't allow an attacker to efficiently compute the private key.

We're left with more general algorithms for solving DLP in the inner automorphism group as discussed in section \ref{group-problems}. The fastest-known method, the index calculus, isn't applicable to groups of this type. More general methods for solving DLP given $g$ and $g^a$ run in $\mathcal{O}(\sqrt{p})$ time, where $p$ is the largest prime factor in the order of $g$. Therefore, we should make sure to select an element $g$ of high and ideally prime order. If we can select $g$ such that its order is a multiple of $p$, the complexity in terms of the bit size $k$ of $p$ would be $\mathcal{O}(2^{k/2})$. This seems to indicate that breaking the encryption by solving DLP is unfeasible.

In \cite{mor_paper}, Paeng et al. argue that if we choose a 160-bit prime the expected number of bit operations for solving DLP in the inner automorphism group is about $2^{87}$, while we would need about $2^{80}$ operations to factor an integer in 1024-bit RSA. Accordingly, the security of the two algorithms should be comparable given these parameter choices.

## Attacks on the cryptosystem {#attacks}

We will now discuss three attacks on MOR using $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ that were discovered in \cite{mor_analysis}. Taken together, these attacks can be successfully deployed to break the cipher in the case of exponent reuse. However, even if exponents are not reused, these attacks may help weaken the cipher so that other attacks might be used instead.

\subsubsection{Reducing \texorpdfstring{$SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$}{SL(2, Fp) x Fp} to \texorpdfstring{$SL(2, \mathbb{F}_p)$}{SL(2, Fp)}} \label{reduction}

If we apply theorem \ref{inn-equality} inductively, we see that
$$
\left.Inn((X,y))^n\right|_{SL(2, \mathbb{F}_p)} = Inn\left(X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}\right)^n
$$
for every $n \in \mathbb{N}$. But this means that using MOR with the group $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ and $g = (X, y)$ is equivalent to using MOR with the group $SL(2, \mathbb{F}_p)$ and $g^\prime = X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix}$. In particular, an attacker can easily derive both $Inn(g^\prime)$ and $Inn(g^\prime)^a$ from the public key. In order to get at the private key $a$, an attacker can try to solve DLP in $Inn(SL(2, \mathbb{F}_p))$. This has two major implications:

First, it means that for the remainder of the discussion, we can focus on attacks against MOR with the group $SL(2, \mathbb{F}_p)$, since they immediately also apply to $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$. Second, it requires a re-evaluation of the DLP algorithm discussed in section \ref{mor-security}. While finding all $g^\prime$ with $Inn(g^\prime) = Inn(g)$ remains easy in $SL(2, \mathbb{F}_p)$, we now know that the solution set is $\pm g$, so the second step of the algorithm is easy too. However, to find $a$ we have to solve DLP in $SL(2, \mathbb{F}_p)$, the difficulty of which depends on the element $g$. As we discussed, more general DLP algorithms have complexity $\mathcal{O}(\sqrt{p})$ where $p$ is the largest prime factor of the order of $g$. If we choose an element of suitable order, then solving DLP in $SL(2, \mathbb{F}_p)$ and, in turn, in the inner automorphism group may be unfeasible by using this method.

However, a careless choice of $g$ can be enough to render the scheme otherwise trivially broken: In \cite{mor_paper}, Paeng et al. suggest using $(X, y) \in SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ such that
$$
g = X\begin{pmatrix}1 & y\\0 & 1\end{pmatrix} = AT^iA^{-1}
$$
for some $i \in \mathbb{N}$ and $A = \begin{pmatrix}x & y\\z & w\end{pmatrix} \in SL(2, \mathbb{F}_p)$. However, as pointed out by \cite{mor_analysis}, DLP can be easily solved for such a $g$ since we showed in the proof of theorem \ref{powers-of-t} that 
$$
AT^kA^{-1} = \begin{pmatrix}1-kxz & kx^2\\-kz^2 & 1 + kxz\end{pmatrix}
$$
for every $k \in \mathbb{N}$, and therefore we can compare $g = AT^iA^{-1}$ and $g^a = AT^{ia}A^{-1}$ in order to easily extract the private key $a$. This emphasises that we have to make sure we choose an element $g \in SL(2, \mathbb{F}_p)$ for which solving DLP is hard.
 
### Deriving information about the structure of the plaintext {#reconstruction}

We will now show that, if we know one entry of the plaintext matrix, we can usually recover the rest of the matrix. We will carry out this attack in MOR over $SL(2, \mathbb{F}_p)$, but as explained in the previous section, this can immediately be applied to $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ as well.

If $A \in SL(2, \mathbb{F}_p)$, then its **trace** $\mathrm{tr}(A)$ is the sum of its diagonal entries. We will show that the trace of a matrix is invariant under conjugation:
\begin{theorem}\label{trace}
Let $A, X \in SL(2, \mathbb{F}_p)$. Then, $\mathrm{tr}(A) = \mathrm{tr}(XAX^{-1})$.
\end{theorem}
\begin{proof}
Write
$$
A = \begin{pmatrix}a & b\\ c & d\end{pmatrix}\quad X = \begin{pmatrix}x & y\\z & w\end{pmatrix}\quad XAX^{-1} = \begin{pmatrix}\hat{a} & \hat{b}\\\hat{c} & \hat{d}\end{pmatrix}.
$$
We immediately get the equations $\hat{a} = axw + cyw - bxz -dyz$ and $\hat{d} = -ayz-cyw+bxz+dxw$, whence $\mathrm{tr}(XAX  ^{-1}) = \hat{a}+\hat{d} = (a+d)(xw-yz) = a + d = \mathrm{tr}(A)$, since $\det(X) = xw-yz = 1$.
 \end{proof}

Now, let's imagine that we're given an encryption $C=X^{ab}AX^{-ab}$ of some matrix ${A = \begin{pmatrix}a & b\\c & d\end{pmatrix}}$. From theorem \ref{trace}, we know that $a + d = \mathrm{tr}(C) := t$. We also know that $ad-bc = 1$. From $Inn(X)$, we may recover $X^\prime = \begin{pmatrix}\hat{x} & \hat{y}\\\hat{z} & \hat{w}\end{pmatrix} = \pm X$ by the method of section \ref{scp-in-slfp}, which commutes with $X$. We can then compute $X^\prime C = X^\prime X^{ab} A X^{-ab} = X^{ab} (X^\prime A)X^{-ab}$ and therefore $\mathrm{tr}(X^\prime A) = \hat{x}a+\hat{y}c + \hat{z}b + \hat{w}d = \mathrm{tr}(X^\prime C) := t^\prime$. So, we have three equations with unknowns $a, b, c, d$:
\begin{align*}
a + d &= t\\
\hat{x}a + \hat{y}c + \hat{z}b + \hat{w}d &= t^\prime\\
ad-bc &= 1.
\end{align*}
If we know one of the components, we will in most cases be able to solve for the other three components. Therefore, knowing just one entry of the matrix will generally allow us to reconstruct the entire matrix.

If, instead of using the random padding method described in section \ref{slfp-rep}, we split a plaintext message across the different entries of the matrix, this would pose a more immediate problem, since knowing only part of the plaintext message would lead us to recover the entirety of it. Using padding as described avoids this problem. However, the results of this section still imply that, whenever we know the plaintext, we can usually reconstruct the entire matrix that goes into the actual MOR encryption, something which we will exploit in the next section.

### Breaking the encryption in case of exponent reuse {#break-reuse}

We discussed in section \ref{modified-mor} the proposal to reuse the exponent $b$ across multiple encryptions. We will now demonstrate why this breaks the encryption under a known-plaintext attack in the case of $SL(2, \mathbb{F}_p)$ (and, by extension, $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$). In fact, we will only need a single plaintext/ciphertext pair.

If we're given a plaintext message and we use the padding method discussed in section \ref{slfp-rep}, we've just shown that we can reconstruct the entire plaintext matrix $M$. If $g = X$ in our encryption scheme, we can also compute $X^\prime = \pm X$ from $Inn(X)$, which is part of the public key. If we know $C_1 := Inn(X^{ab})(M)$, then we have that $C_2 := X^\prime C_1 = Inn(X^{ab})(X^\prime M)$, so we have two instances $(M, C_1), (X^\prime M, C_2)$ of the conjugacy problem in $SL(2, \mathbb{F}_p)$. We can assume that $X^\prime M \notin Z(M)$---otherwise we'd have $X^\prime \in Z(M)$, but then $Inn(X^{ab})(M) = M$, so the system would be trivially broken. By section \ref{scp-in-slfp}, this is enough to recover $\pm X^{ab}$. But then, we can also easily compute $Inn(X^{-ab})$, which means that we can decrypt any message.

In order to prevent this sort of attack in the context of exponent reuse, it would be necessary to use a different padding technique, such as adding random padding to every matrix entry. In that case, it might not be possible to reconstruct the entire matrix just by knowing the plaintext message. Even then, this doesn't guarantee the security of the system.
