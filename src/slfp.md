\section{The group \texorpdfstring{$SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$}{SL(2, Fp) x Fp}}\label{slfp}

We will now apply the MOR cryptosystem to one particular type of group, $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$, that was chosen as an example by Paeng et al. in \cite{mor_paper}. This group is a semidirect product with the map $\theta$ being given by[^1]
$$
\theta(n)(A) = \begin{pmatrix}1 & n\\0 & 1\end{pmatrix}A\begin{pmatrix}1 & -n\\0 & 1\end{pmatrix}.
$$
In other words, the elements of this group are of the form $(A, n)$, where $A$ is a $2\times 2$ matrix over the finite field $\mathbb{F}_p$ with determinant $1$ and $n$ is an element of $\mathbb{F}_p$. The multiplication is then given by
$$
(A, n)\cdot (B, m) = \left(A\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}B\begin{pmatrix}1 & -n\\0 & 1\end{pmatrix}, n+m\right).
$$

\begin{remark}
The following identities can immediately be shown by direct computation or induction:
\begin{enumerate}
\item $\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}\begin{pmatrix}1 & m\\0 & 1\end{pmatrix} = \begin{pmatrix}1 & n+m\\0 & 1\end{pmatrix}$.
\item $\begin{pmatrix}1 & 1\\0 & 1\end{pmatrix}^n = \begin{pmatrix}1 & n\\0 & 1\end{pmatrix}$.
\item $\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}^{-1} = \begin{pmatrix}1 & -n\\0 & 1\end{pmatrix} = \begin{pmatrix}1 & p-n\\0 & 1\end{pmatrix}$.
\end{enumerate}

We also immediately see that in $SL(2,\mathbb{F}_p)\times_\theta \mathbb{F}_p$ the identity element is $(I, 0)$ and the inverse of any element $(A, n)$ is given by
$$
\left(\begin{pmatrix}1 & -n\\0 & 1\end{pmatrix}A^{-1}\begin{pmatrix}0 & n\\0 & 1\end{pmatrix}, -n\right).
$$
\end{remark}

We have to show that $\theta$ satisfies the conditions required for the definition of a semidirect product. If $\det(A) = 1$, then $\det(\theta(n)(A)) = 1$ for any $n \in \mathbb{F}_p$, so $\theta(n)$ is a map from $SL(2, \mathbb{F}_p)$ to itself. Since $\theta(n)(A+B)=\theta(n)(A)+\theta(n)(B)$, it is a homomorphism, and since an inverse is given by $\theta(-n)$, it is an automorphism. It remains to show that $\theta$ itself is a homomorphism, but this follows from property 1 above.

We also have to decide which element $g \in SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ to use during encryption. Which element we choose will have some implications in term of security, so we will revisit this point in section \ref{dlp-in-inn}.

## Representing messages {#slfp-rep}

According to theorem \ref{semidirect-subgroup}, the group $SL(2, \mathbb{F}_p)$ is a normal subgroup of $SL(2,\mathbb{F}_p)\times_\theta \mathbb{F}_p$. Therefore, we will choose to represent plaintext (and ciphertext) messages as elements of this subgroup. This means that we need to encode plaintext messages as matrices.

Let us first assume that our original message $M$ is an element of $\mathbb{F}_p$: We can simply take the binary encoding of $M$ and reinterpret it as the binary representation of a particular number.[^binary] Let us also assume that $M\neq 0$. During encryption, Alice chooses two elements $r_1, r_2 \in \mathbb{F}_p$ uniformly at random and constructs the matrix

[^binary]: This works as long as the bit-size of $M$ is not larger than $\log_2 p$. If MOR is used to exchange e.g. the key for a symmetric cipher, which is typically a fixed-size binary string, this should not be an issue.

$$
\begin{pmatrix}M & r_1\\r_2 & \frac{1+r_1r_2}{M}\end{pmatrix}
$$
whose determinant is $1$. She can therefore encrypt this matrix using MOR. When Bob decrypts the message, he will simply ignore any of the entries except the one at $(1,1)$. In this combined scheme, encryption is non-deterministic even if a precomputed value of $b$ is being used. This is a benefit since theorem \ref{deterministic-insecure} showed that a public-key cipher can't be secure if it is deterministic.

Furthermore, Paeng et al. claim that this solves one of the problems we have observed in conjuction with the original MOR cryptosystem: that the plaintext and the ciphertext are in the same conjugacy class. While this sounds plausible, they haven't actually shown that choosing $r_1, r_2$ uniformly at random distributes the plaintext matrix uniformly among the different conjugacy classes.

## Efficiency of encryption and decryption {#efficiency}

We now want to discuss how the requisite computations for encryption and decryption are carried out in practice and whether this can be done efficiently. First, we'll have to exibit a set of generators for $SL(2,\mathbb{F}_p)$:[^2]

\begin{theorem}\label{decompose}
Let
$$
T = \begin{pmatrix}1 & 1\\0 & 1\end{pmatrix},\quad S = \begin{pmatrix}0 & -1\\1 & 0\end{pmatrix}.
$$
Then, for every element $A \in SL(2,\mathbb{F}_p)$ there exist $i \in \{0,1\}$ and $j, k, l \in \mathbb{Z}$ such that
$$
A = S^iT^jST^kST^l.
$$
\end{theorem}
\begin{proof}
Let's first assume that $a_{21}\neq 0$. We can easily compute
$$
T^jST^kST^l = \begin{pmatrix}kj-1 & kjl-j-l\\k & kl-1\end{pmatrix}.
$$
If we set this matrix equal to $A$, we have a system of four equations with three unknowns. Since $a_{21}$ is non-zero, $k$ is not zero either. We can then solve $kj-1 = a_{11}$ and $kl-1 = a_{22}$ for $j$ and $l$, respectively (which will be integers because $\mathbb{F}_p$ is the field of integers mod $p$). But there can only be a single matrix in $SL(2, \mathbb{F}_p)$ that agrees with $A$ in the entries $a_{11}, a_{21}$ and $a_{22}$, because we can easily solve $a_{11}a_{22}-a_{12}a_{21}=1$ for $a_{12}$ (again, since $a_{21} \neq 0$). Since $T^jST^kST^l \in SL(2,\mathbb{F}_p)$, it must be equal to $A$.

If $a_{21}=0$, then $a_{11}\neq 0$ (otherwise the determinant would be $0$). Then,
$$
S^{-1}A = \begin{pmatrix}0 & 1\\-1 & 0\end{pmatrix}\begin{pmatrix}a_{11} & a_{12}\\a_{21} & a_{22}\end{pmatrix} = \begin{pmatrix}a_{21} & a_{22}\\-a_{11} & -a_{12}\end{pmatrix}
$$
and so the $(2,1)$-entry of $S^{-1}A$ is non-zero. We can then find a decomposition $T^jST^kST^l = S^{-1}A$ and therefore $ST^jST^kST^l = A$.
\end{proof}
The above proof is constructive, therefore we can find the decomposition of any matrix with a constant number of arithmetic operations.

We now discuss how we can compute $Inn(g)^n$ given $Inn(g)$ and $n \in \mathbb{Z}$, a computation that is carried out multiple times during encryption and decryption. We are given $Inn(g)(S)$ and $Inn(g)(T)$ and we are looking for $Inn(g)^n(S)$ and $Inn(g)^n(T)$, which uniquely identify $Inn(g)^n$. We can now find the decompositions $Inn(g)(S)=S^{i_1}T^{j_1}ST^{k_1}ST^{l_1}$ and $Inn(g)(T)=S^{i_2}T^{j_2}ST^{k_2}ST^{l_2}$. Then we have that
\begin{align*}
&\quad Inn(g)^2(S)\\
= &\quad Inn(g)(S^{i_1}T^{j_1}ST^{k_1}ST^{l_1})\\
= &\quad\left(Inn(g)(S)\right)^{i_1}\left(Inn(g)(T)\right)^{j_1}Inn(g)(S)\left(Inn(g)(T)\right)^{k_1}Inn(g)(S)\left(Inn(g)(T)\right)^{l_1}
\end{align*}
and
\begin{align*}
&\quad Inn(g)^2(T)\\
= &\quad Inn(g)(S^{i_2}T^{j_2}ST^{k_2}ST^{l_2})\\
= &\quad\left(Inn(g)(S)\right)^{i_2}\left(Inn(g)(T)\right)^{j_2}Inn(g)(S)\left(Inn(g)(T)\right)^{k_2}Inn(g)(S)\left(Inn(g)(T)\right)^{l_2}.
\end{align*}

These two matrices can now be calculated from $Inn(g)(S)$ and $Inn(g)(T)$. We run the decomposition algorithm one more time to find ${Inn(g)^2(S)=S^{i^\prime_1}T^{j^\prime_1}ST^{k^\prime_1}ST^{l^\prime_1}}$ and ${Inn(g)^2(T)=S^{i^\prime_2}T^{j^\prime_2}ST^{k^\prime_2}ST^{l^\prime_2}}$. Now, we have again
\begin{align*}
&\quad Inn(g)^3(S)\\
= &\quad Inn(g)(S^{i^\prime_1}T^{j^\prime_1}ST^{k^\prime_1}ST^{l^\prime_1})\\
= &\quad\left(Inn(g)(S)\right)^{i^\prime_1}\left(Inn(g)(T)\right)^{j^\prime_1}Inn(g)(S)\left(Inn(g)(T)\right)^{k^\prime_1}Inn(g)(S)\left(Inn(g)(T)\right)^{l^\prime_1}
\end{align*}
and analogously for $Inn(g)^3(T)$. By iterating this procedure, we can eventually find $Inn(g)^n(S)$ and $Inn(g)^n(T)$.

The above computation can be sped up by two additional techniques. The first relies on the following theorem (remember that $Inn(g)(S)^i$ is always easy to compute since $i$ is either $0$ or $1$):
\begin{theorem}\label{powers-of-t}
If
$$
Inn(g)(T) = \begin{pmatrix}x & y\\z & w\end{pmatrix},
$$
then
$$
\left(Inn(g)(T)\right)^n = \begin{pmatrix}1 - n(1 - x) & ny\\nz & 1 + n(w-1)\end{pmatrix}.
$$
\end{theorem}
\begin{proof}
Since $Inn(g)$ is a homomorphism,
$$
\left(Inn(g)(T)\right)^n = Inn(g)(T^n) = Inn(g)\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}.
$$
If $g =: (A, m)$, then
\begin{align*}
&(A,m)\left(\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}, 0\right)(A,m)^{-1} \\
= \quad&\left(A\begin{pmatrix}1 & m\\0 & 1\end{pmatrix}\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -m\\0 & 1\end{pmatrix}, m\right)\left(\begin{pmatrix}1 & -m\\0 & 1\end{pmatrix}A^{-1}\begin{pmatrix}1 & m\\0 & 1\end{pmatrix}, -m\right)\\
= \quad&\left(A\begin{pmatrix}1 & n\\0 & 1\end{pmatrix}\begin{pmatrix}1 & m\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -m\\0 & 1\end{pmatrix}A^{-1}\begin{pmatrix}1 & m\\0 & 1\end{pmatrix}\begin{pmatrix}1 & -m\\0 & 1\end{pmatrix}, 0\right)\\
= \quad&(AT^nA^{-1}, 0),
\end{align*}
therefore we can identify $Inn(g)(T^n)$ with $AT^nA^{-1}$.

Let's now write $A = \begin{pmatrix}a & b\\c & d\end{pmatrix}$. Then,
$$
AT^kA^{-1} = \begin{pmatrix}a & b\\c & d\end{pmatrix}\begin{pmatrix}1 & k\\0 & 1\end{pmatrix}\begin{pmatrix}d & -b\\-c & a\end{pmatrix} = \begin{pmatrix}1-kac & ka^2\\-kc^2 & 1+kac\end{pmatrix}
$$
for any $k \in \mathbb{Z}$ (since $ad-bc=1$), and in particular
\begin{align*}
ATA^{-1} &= \begin{pmatrix}1 - ac & a^2\\-c^2 & 1 + ac\end{pmatrix} = \begin{pmatrix}x & y \\ z & w\end{pmatrix}\\
AT^nA^{-1} &= \begin{pmatrix}1 - nac & na^2\\-nc^2 & 1 + nac\end{pmatrix} = \begin{pmatrix}1 - n(1-x) & ny\\nz & 1+n(w-1)\end{pmatrix}.
\end{align*}
\end{proof}

Again, this allows us to find $\left(Inn(g)(T)\right)^n$ with a constant number of arithmetic operations.

The second technique uses *repeated squaring* \cite[pp.~553--555]{crypto_book}: Instead of computing, $Inn(g)^2$, $Inn(g)^3$, etc., we compute only the powers of two. This works because
$$
Inn(g)^{2^k m}(S) = Inn(g)^{2^k}(Inn(g)^{m}(S)) = \left(Inn(g)^{2^k}(S)\right)^i\cdots\left(Inn(g)^{2^k}(T)\right)^l
$$
if $(i,j,k,l)$ is a decomposition of $Inn(g)^m(S)$. We can then compute $Inn(g)^n(S)$ by decomposing $n$ into a product of powers of two (i.e. its binary representation), for example $Inn(g)^{11} = Inn(g)^8 \circ Inn(g)^2 \circ Inn(g)$. The same principle applies to computing $Inn(g)^n(T)$. The full algorithm for computing $Inn(g)^n$ is given on the next page.

\begin{algorithm}
\caption{Computing powers of $Inn(g)$}
\textbf{Input:}
\begin{itemize}
\tightlist
\item $g_S$: $Inn(g)(S)$
\item $g_T$: $Inn(g)(T)$
\item $n \in \mathbb{N}$
\end{itemize}

\textbf{Output:} $\left(Inn(g)^n(S), Inn(g)^n(T)\right)$

\textbf{Constants:} $S, T$

\textbf{Subroutines:}
\begin{itemize}
\tightlist
\item \verb|bin_array|: converts a natural number into an array of binary positions.
\item \verb|decompose|: for an element of $SL(2, \mathbb{F}_p)$, it finds $i,j,k,l$ according to theorem \ref{decompose}.
\item \verb|pow|: Compute powers of matrices according to theorem \ref{powers-of-t}.
\end{itemize}
\begin{algorithmic}[1]
\Procedure{InnPower}{$g_S, g_T, n$}
\State $[b_m, \ldots, b_0] \gets \verb|bin_array|(n)$
\State $g^0_S \gets g_S$
\State $g^0_T \gets g_T$
\If{$b_0 = 1$}
\State $r_S \gets g_S$
\State $r_T \gets g_T$
\Else
\State $r_S \gets S$
\State $r_T \gets T$
\EndIf
\For{$k = 1\ldots m$}
\State $(i,j,k,l) \gets \verb|decompose|(g^{k-1}_S)$
\State $g^k_S \gets \verb|pow|(g^{k-1}_S, i)\ast \verb|pow|(g^{k-1}_T, j) \ast g^{k-1}_S \ast \verb|pow|(g^{k-1}_T, k) \ast g^{k-1}_S \ast \verb|pow|(g^{k-1}_T, l)$
\item[]
\State $(i,j,k,l) \gets \verb|decompose|(g^{k-1}_T)$
\State $g^k_T \gets \verb|pow|(g^{k-1}_S, i)\ast \verb|pow|(g^{k-1}_T, j) \ast g^{k-1}_S \ast \verb|pow|(g^{k-1}_T, k) \ast g^{k-1}_S \ast \verb|pow|(g^{k-1}_T, l)$
\item[]
\If{$b_k = 1$}
\State $(i,j,k,l) \gets \verb|decompose|(r_S)$
\State $r_S \gets \verb|pow|(g^k_S, i)\ast \verb|pow|(g^k_T, j) \ast g^k_S \ast \verb|pow|(g^k_T, k) \ast g^k_S \ast \verb|pow|(g^k_T, l)$
\item[]
\State $(i,j,k,l) \gets \verb|decompose|(r_T)$
\State $r_T \gets \verb|pow|(g^k_S, i)\ast \verb|pow|(g^k_T, j) \ast g^k_S \ast \verb|pow|(g^k_T, k) \ast g^k_S \ast \verb|pow|(g^k_T, l)$
\EndIf
\EndFor
\State \Return $(r_S, r_T)$
\EndProcedure
\end{algorithmic}
\end{algorithm}

Within the loop of this algorithm, the values $g^k_S$ and $g^k_T$ that we compute are equal to $Inn(g)^{2^k}(S)$ and $Inn(g)^{2^k}(T)$ respectively. After the $k$-th iteration of the loop, we have that $r_S = Inn(g)^{\sum_{i=0}^k b_i 2^i}(S)$ and $r_T = Inn(g)^{\sum_{i=0}^k b_i 2^i}(T)$. Therefore, at the end of the algorithm, $r_S = Inn(g)^n(S)$ and $r_T = Inn(g)^n(T)$.

As we have shown, all the computations within a single iteration of the loop can be carried out with a constant number of arithmetic operations, therefore in total the procedure will have to execute $\mathcal{O}(\log_2(n))$ operations. Let us assume that $k \approx \log_2(p)$ is the bit-size of $p$. We know from theorem \ref{slfp-order} that $SL(2, \mathbb{F}_p)$ has order $p^3-p$ and so the order of $SL(2, \mathbb{F}_p) \times_\theta \mathbb{F}_p$ is $p^4 - p^2$. Therefore, the maximum order of any element in this group is also $p^4-p^2 < p^4$ and there is no value in choosing any $n$ larger than that. We can therefore assume that the maximum bit size of an exponent $n$ is about $4\log_2(p) \in \mathcal{O}(k)$. The bit complexity of the exponentation algorithm, as a function of $k$, is then $\mathcal{O}(kM(k))$, where $M(k)$ is the complexity of multiplying $k$-bit numbers modulo $p$. The function $M(k)$ runs in time roughly $\mathcal{O}(k^2)$ \cite{arithmetic}, which means that encryption and decryption are asymptotically efficient.

According to Paeng et al., the exact number of multiplications within one iteration of the loop is $92$. Therefore, since we need to compute both $Inn(g^a)^b$ and $Inn(g)^b$, the number of bit operations during encryption is approximately proportional to $184k^3$. As we will discuss in section \ref{dlp-in-inn}, Paeng et al. claim that using their system with a 160 bit prime $p$ makes it about as secure as using RSA with a 1024 bit key. This would mean that, during encryption, the number of bit operations would be proportional to $184\cdot 160^3 \approx 8\cdot 10^8$, whereas the number of operations during RSA encryption is supposedly proportional to ${1024^3 \approx 10^9}$. This would suggest that the two cryptosystems are approximately equally efficient. However, these are very rough calculations. It is not clear whether comparing the two systems in this way, without benchmarking concrete implementations in practice, is valid, since runtimes might also depend on implementation and machine specific behaviour, such as software and hardware optimisations. In fact, it is claimed in \cite{mor_analysis} that the regular MOR cryptosystem, in which $b$ is not precomputed, is less efficient than RSA, although no evidence is provided. Even if we accept that the MOR cryptosystem is about as or even slightly more efficient than RSA, it is unlikely to be more efficient than ECC, which is generally faster than RSA.

On the other hand, if $b$ is precomputed, then it is only necessary to apply an inner automorphism to a matrix during encryption and decryption. By applying theorems \ref{decompose} and \ref{powers-of-t} again, we see that in this case, only a constant number of multiplications need to be carried out. This makes encryption and decryption significantly more efficient than in both RSA and ECC, especially with large keys---for example, it is claimed that decryption is about 200 times faster than in RSA and 40 times faster than in ECC (although Paeng et al. don't specify which particular ECC cipher they have in mind).

Finally, we briefly compare the key lengths in RSA and MOR. If we use a 160 bit prime number, then $6\cdot 160 = 960$ bit are needed to encode both $Inn(g^a)(S)$ and $Inn(g^a)(T)$, since three entries are enough to uniquely determine a matrix in $SL(2, \mathbb{F}_p)$. If $g$ is a fixed parameter of the algorithm, then $Inn(g)$ doesn't need to be part of the public key, since it can be precomputed. Hence, we only need 960 bit to store the public key, while for storing the private key, $a \in \mathbb{F}_p$, only 160 bit are needed. This is less than is needed for 1024 bit RSA which, as it is claimed, offers a comparable amount of security.

[^1]: The construction in \cite{mor_paper} is slightly more abstract in its choice of $\theta$, but this doesn't appear to substantially alter our analysis here.

[^2]: The original theorem in \cite{mor_paper} proves that $A = T^jST^kST^l$ for any $A$ with $a_{21}\neq 0$. However, Paeng et al. provide no proof that only such elements with a non-zero $(2,1)$-entry appear within the encryption and decryption algorithms, so we've opted here for the slightly more general statement.
